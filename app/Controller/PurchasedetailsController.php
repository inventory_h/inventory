<?php
App::uses('AppController', 'Controller');
/**
 * Purchasedetails Controller
 *
 * @property Purchasedetail $Purchasedetail
 * @property PaginatorComponent $Paginator
 */
class PurchasedetailsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Purchasedetail->recursive = 0;
		$this->set('purchasedetails', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Purchasedetail->exists($id)) {
			throw new NotFoundException(__('Invalid purchasedetail'));
		}
		$options = array('conditions' => array('Purchasedetail.' . $this->Purchasedetail->primaryKey => $id));
		$this->set('purchasedetail', $this->Purchasedetail->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Purchasedetail->create();
			if ($this->Purchasedetail->save($this->request->data)) {
				$this->Flash->success(__('The purchasedetail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The purchasedetail could not be saved. Please, try again.'));
			}
		}
		$purchases = $this->Purchasedetail->Purchase->find('list');
		$products = $this->Purchasedetail->Product->find('list');
		$units = $this->Purchasedetail->Unit->find('list');
		$this->set(compact('purchases', 'products', 'units'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Purchasedetail->exists($id)) {
			throw new NotFoundException(__('Invalid purchasedetail'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Purchasedetail->save($this->request->data)) {
				$this->Flash->success(__('The purchasedetail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The purchasedetail could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Purchasedetail.' . $this->Purchasedetail->primaryKey => $id));
			$this->request->data = $this->Purchasedetail->find('first', $options);
		}
		$purchases = $this->Purchasedetail->Purchase->find('list');
		$products = $this->Purchasedetail->Product->find('list');
		$units = $this->Purchasedetail->Unit->find('list');
		$this->set(compact('purchases', 'products', 'units'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Purchasedetail->id = $id;
		if (!$this->Purchasedetail->exists()) {
			throw new NotFoundException(__('Invalid purchasedetail'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Purchasedetail->delete()) {
			$this->Flash->success(__('The purchasedetail has been deleted.'));
		} else {
			$this->Flash->error(__('The purchasedetail could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
