<?php
App::uses('AppController', 'Controller');
/**
 * Accountrecives Controller
 *
 * @property Accountrecife $Accountrecife
 * @property PaginatorComponent $Paginator
 */
class AccountrecivesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Accountrecife->recursive = 0;
		$this->set('accountrecives', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Accountrecife->exists($id)) {
			throw new NotFoundException(__('Invalid accountrecife'));
		}
		$options = array('conditions' => array('Accountrecife.' . $this->Accountrecife->primaryKey => $id));
		$this->set('accountrecife', $this->Accountrecife->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Accountrecife->create();
			if ($this->Accountrecife->save($this->request->data)) {
				$this->Flash->success(__('The accountrecife has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The accountrecife could not be saved. Please, try again.'));
			}
		}
		$customers = $this->Accountrecife->Customer->find('list');
		$this->set(compact('customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Accountrecife->exists($id)) {
			throw new NotFoundException(__('Invalid accountrecife'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Accountrecife->save($this->request->data)) {
				$this->Flash->success(__('The accountrecife has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The accountrecife could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Accountrecife.' . $this->Accountrecife->primaryKey => $id));
			$this->request->data = $this->Accountrecife->find('first', $options);
		}
		$customers = $this->Accountrecife->Customer->find('list');
		$this->set(compact('customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Accountrecife->id = $id;
		if (!$this->Accountrecife->exists()) {
			throw new NotFoundException(__('Invalid accountrecife'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Accountrecife->delete()) {
			$this->Flash->success(__('The accountrecife has been deleted.'));
		} else {
			$this->Flash->error(__('The accountrecife could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
