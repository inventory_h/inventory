<?php
App::uses('AppController', 'Controller');
/**
 * Paymentdetails Controller
 *
 * @property Paymentdetail $Paymentdetail
 * @property PaginatorComponent $Paginator
 */
class PaymentdetailsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Paymentdetail->recursive = 0;
		$this->set('paymentdetails', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Paymentdetail->exists($id)) {
			throw new NotFoundException(__('Invalid paymentdetail'));
		}
		$options = array('conditions' => array('Paymentdetail.' . $this->Paymentdetail->primaryKey => $id));
		$this->set('paymentdetail', $this->Paymentdetail->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Paymentdetail->create();
			if ($this->Paymentdetail->save($this->request->data)) {
				$this->Flash->success(__('The paymentdetail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The paymentdetail could not be saved. Please, try again.'));
			}
		}
		$paymenttypes = $this->Paymentdetail->Paymenttype->find('list');
		$sales = $this->Paymentdetail->Sale->find('list');
		$this->set(compact('paymenttypes', 'sales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Paymentdetail->exists($id)) {
			throw new NotFoundException(__('Invalid paymentdetail'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Paymentdetail->save($this->request->data)) {
				$this->Flash->success(__('The paymentdetail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The paymentdetail could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Paymentdetail.' . $this->Paymentdetail->primaryKey => $id));
			$this->request->data = $this->Paymentdetail->find('first', $options);
		}
		$paymenttypes = $this->Paymentdetail->Paymenttype->find('list');
		$sales = $this->Paymentdetail->Sale->find('list');
		$this->set(compact('paymenttypes', 'sales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Paymentdetail->id = $id;
		if (!$this->Paymentdetail->exists()) {
			throw new NotFoundException(__('Invalid paymentdetail'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Paymentdetail->delete()) {
			$this->Flash->success(__('The paymentdetail has been deleted.'));
		} else {
			$this->Flash->error(__('The paymentdetail could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
