<?php
App::uses('AppController', 'Controller');
/**
 * Productmodels Controller
 *
 * @property Productmodel $Productmodel
 * @property PaginatorComponent $Paginator
 */
class ProductmodelsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Productmodel->recursive = 0;
		$this->set('productmodels', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Productmodel->exists($id)) {
			throw new NotFoundException(__('Invalid productmodel'));
		}
		$options = array('conditions' => array('Productmodel.' . $this->Productmodel->primaryKey => $id));
		$this->set('productmodel', $this->Productmodel->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			
			/*echo '<pre>';
			print_r($this->request->data);
			echo '</pre>';die();*/
			
			$this->Productmodel->create();
			
			if ($this->Productmodel->save($this->request->data)) {
				//$this->Flash->success(__('The productmodel has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The productmodel could not be saved. Please, try again.'));
			}
		}
		$brands = $this->Productmodel->Brand->find('list');
		$this->set(compact('brands'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Productmodel->exists($id)) {
			throw new NotFoundException(__('Invalid productmodel'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Productmodel->save($this->request->data)) {
				//$this->Flash->success(__('The productmodel has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The productmodel could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Productmodel.' . $this->Productmodel->primaryKey => $id));
			$this->request->data = $this->Productmodel->find('first', $options);
		}
		$brands = $this->Productmodel->Brand->find('list');
		$this->set(compact('brands'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Productmodel->id = $id;
		if (!$this->Productmodel->exists()) {
			throw new NotFoundException(__('Invalid productmodel'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Productmodel->delete()) {
			//$this->Flash->success(__('The productmodel has been deleted.'));
		} else {
			//$this->Flash->error(__('The productmodel could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	public function dropdown2(){
		$this->layout='ajax';
		
		 if(!empty($_REQUEST['brand_id'])){
		 	
		 		$this->set('productmodels',$this->Productmodel->find('list',array('fields'=>array('id','name'),'conditions' =>
                        array('brand_id'=>$_REQUEST['brand_id']),
                        'recursive' => -1)));
            
		 }
		
	}
}
