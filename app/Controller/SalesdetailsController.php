<?php
App::uses('AppController', 'Controller');
/**
 * Salesdetails Controller
 *
 * @property Salesdetail $Salesdetail
 * @property PaginatorComponent $Paginator
 */
class SalesdetailsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Salesdetail->recursive = 0;
		$this->set('salesdetails', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Salesdetail->exists($id)) {
			throw new NotFoundException(__('Invalid salesdetail'));
		}
		$options = array('conditions' => array('Salesdetail.' . $this->Salesdetail->primaryKey => $id));
		$this->set('salesdetail', $this->Salesdetail->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Salesdetail->create();
			if ($this->Salesdetail->save($this->request->data)) {
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The salesdetail could not be saved. Please, try again.'));
			}
		}
		$sales = $this->Salesdetail->Sale->find('list');
		$products = $this->Salesdetail->Product->find('list');
		$units = $this->Salesdetail->Unit->find('list');
		$paymenttypes = $this->Salesdetail->Paymenttype->find('list');
		$this->set(compact('sales', 'products', 'units', 'paymenttypes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Salesdetail->exists($id)) {
			throw new NotFoundException(__('Invalid salesdetail'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Salesdetail->save($this->request->data)) {
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The salesdetail could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Salesdetail.' . $this->Salesdetail->primaryKey => $id));
			$this->request->data = $this->Salesdetail->find('first', $options);
		}
		$sales = $this->Salesdetail->Sale->find('list');
		$products = $this->Salesdetail->Product->find('list');
		$units = $this->Salesdetail->Unit->find('list');
		$paymenttypes = $this->Salesdetail->Paymenttype->find('list');
		$this->set(compact('sales', 'products', 'units', 'paymenttypes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Salesdetail->id = $id;
		if (!$this->Salesdetail->exists()) {
			throw new NotFoundException(__('Invalid salesdetail'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Salesdetail->delete()) {
			$this->Flash->success(__('The salesdetail has been deleted.'));
		} else {
			$this->Flash->error(__('The salesdetail could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
