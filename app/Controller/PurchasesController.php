<?php

App::uses('AppController', 'Controller');

/**
 * Purchases Controller
 *
 * @property Purchase $Purchase
 * @property PaginatorComponent $Paginator
 */
class PurchasesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Purchase->recursive = 0;
        $this->set('purchases', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Purchase->exists($id)) {
            throw new NotFoundException(__('Invalid purchase'));
        }
        $options = array('conditions' => array('Purchase.' . $this->Purchase->primaryKey => $id));
        $this->set('purchase', $this->Purchase->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->loadModel('Product');
        $this->loadModel('Stock');
        $this->loadModel('Unit');
        if ($this->request->is('post')) {

            foreach ($this->request->data['Purchasedetail'] as $purinfo) {
                $pid = $purinfo['product_id'];
                $pqty = $purinfo['qty'];
                $unit = $purinfo['unit_id'];
         
                $price = $purinfo['price'];
          
               
               
                
               if (!$this->Stock->exists($pid)) {
                    $this->Purchase->query("INSERT INTO `stocks` (`id`,`stockqty`,`soldqty`,`product_id`,`oldPrice`,`curretPrice`,`unit_id`) VALUES ('NULL',$pqty,0,$pid,450,$price,$unit)");
                } else {
                    $this->Purchase->query("UPDATE stocks SET `stockqty`=`stockqty`+'" . $pqty . "' WHERE product_id=$pid");
                    
                }
            }
 

            $this->Purchase->create();

            if ($this->Purchase->saveAssociated($this->request->data)) {
                //$this->Session->setFlash(__('The purchase has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                //$this->Session->setFlash(__('The purchase could not be saved. Please, try again.'));
            }
        }
        $products = $this->Product->find('list', array('fields' => array('id', 'name')));
        $paymenttypes = $this->Purchase->Paymenttype->find('list');
        $suppliers = $this->Purchase->Supplier->find('list', array('fields' => array('id', 'companyName')));
        $units = $this->Unit->find('list', array('fields' => array('id', 'name')));
        $this->set(compact('products', 'paymenttypes', 'suppliers', 'units'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->loadModel('Product');
        if (!$this->Purchase->exists($id)) {
            throw new NotFoundException(__('Invalid purchase'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Purchase->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('The purchase has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The purchase could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Purchase.' . $this->Purchase->primaryKey => $id));
            $this->request->data = $this->Purchase->find('first', $options);
        }
        $products = $this->Purchase->Product->find('list', array('fields' => array('id', 'name')));
        $paymenttypes = $this->Purchase->Paymenttype->find('list');
        $suppliers = $this->Purchase->Supplier->find('list');
        $this->set(compact('products', 'paymenttypes', 'suppliers'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Purchase->id = $id;
        if (!$this->Purchase->exists()) {
            throw new NotFoundException(__('Invalid purchase'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Purchase->delete()) {
            $this->Flash->success(__('The purchase has been deleted.'));
        } else {
            $this->Flash->error(__('The purchase could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
