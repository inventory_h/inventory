<?php
App::uses('AppController', 'Controller');
/**
 * Purchasereturns Controller
 *
 * @property Purchasereturn $Purchasereturn
 * @property PaginatorComponent $Paginator
 */
class PurchasereturnsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Purchasereturn->recursive = 0;
		$this->set('purchasereturns', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Purchasereturn->exists($id)) {
			throw new NotFoundException(__('Invalid purchasereturn'));
		}
		$options = array('conditions' => array('Purchasereturn.' . $this->Purchasereturn->primaryKey => $id));
		$this->set('purchasereturn', $this->Purchasereturn->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Purchasereturn->create();
			if ($this->Purchasereturn->save($this->request->data)) {
				$this->Flash->success(__('The purchasereturn has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The purchasereturn could not be saved. Please, try again.'));
			}
		}
		$products = $this->Purchasereturn->Product->find('list');
		$suppliers = $this->Purchasereturn->Supplier->find('list');
		$this->set(compact('products', 'suppliers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Purchasereturn->exists($id)) {
			throw new NotFoundException(__('Invalid purchasereturn'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Purchasereturn->save($this->request->data)) {
				$this->Flash->success(__('The purchasereturn has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The purchasereturn could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Purchasereturn.' . $this->Purchasereturn->primaryKey => $id));
			$this->request->data = $this->Purchasereturn->find('first', $options);
		}
		$products = $this->Purchasereturn->Product->find('list');
		$suppliers = $this->Purchasereturn->Supplier->find('list');
		$this->set(compact('products', 'suppliers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Purchasereturn->id = $id;
		if (!$this->Purchasereturn->exists()) {
			throw new NotFoundException(__('Invalid purchasereturn'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Purchasereturn->delete()) {
			$this->Flash->success(__('The purchasereturn has been deleted.'));
		} else {
			$this->Flash->error(__('The purchasereturn could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
