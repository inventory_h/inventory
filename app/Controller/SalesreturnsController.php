<?php
App::uses('AppController', 'Controller');
/**
 * Salesreturns Controller
 *
 * @property Salesreturn $Salesreturn
 * @property PaginatorComponent $Paginator
 */
class SalesreturnsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Salesreturn->recursive = 0;
		$this->set('salesreturns', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Salesreturn->exists($id)) {
			throw new NotFoundException(__('Invalid salesreturn'));
		}
		$options = array('conditions' => array('Salesreturn.' . $this->Salesreturn->primaryKey => $id));
		$this->set('salesreturn', $this->Salesreturn->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('Product');
		$this->loadModel('Customer');
		if ($this->request->is('post')) {
			$this->Salesreturn->create();
			if ($this->Salesreturn->save($this->request->data)) {
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->Flash->error(__('The salesreturn could not be saved. Please, try again.'));
			}
		}
		$products = $this->Product->find('list');
		$customers = $this->Customer->find('list');
		$this->set(compact('products', 'customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Salesreturn->exists($id)) {
			throw new NotFoundException(__('Invalid salesreturn'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Salesreturn->save($this->request->data)) {
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The salesreturn could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Salesreturn.' . $this->Salesreturn->primaryKey => $id));
			$this->request->data = $this->Salesreturn->find('first', $options);
		}
		$products = $this->Salesreturn->Product->find('list');
		$customers = $this->Salesreturn->Customer->find('list');
		$this->set(compact('products', 'customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Salesreturn->id = $id;
		if (!$this->Salesreturn->exists()) {
			throw new NotFoundException(__('Invalid salesreturn'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Salesreturn->delete()) {
			$this->Flash->success(__('The salesreturn has been deleted.'));
		} else {
			$this->Flash->error(__('The salesreturn could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
