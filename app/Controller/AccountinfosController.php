<?php
App::uses('AppController', 'Controller');
/**
 * Accountinfos Controller
 *
 * @property Accountinfo $Accountinfo
 * @property PaginatorComponent $Paginator
 */
class AccountinfosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Accountinfo->recursive = 0;
		$this->set('accountinfos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Accountinfo->exists($id)) {
			throw new NotFoundException(__('Invalid accountinfo'));
		}
		$options = array('conditions' => array('Accountinfo.' . $this->Accountinfo->primaryKey => $id));
		$this->set('accountinfo', $this->Accountinfo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Accountinfo->create();
			if ($this->Accountinfo->save($this->request->data)) {
				$this->Flash->success(__('The accountinfo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The accountinfo could not be saved. Please, try again.'));
			}
		}
		$sales = $this->Accountinfo->Sale->find('list');
		$this->set(compact('sales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Accountinfo->exists($id)) {
			throw new NotFoundException(__('Invalid accountinfo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Accountinfo->save($this->request->data)) {
				$this->Flash->success(__('The accountinfo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The accountinfo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Accountinfo.' . $this->Accountinfo->primaryKey => $id));
			$this->request->data = $this->Accountinfo->find('first', $options);
		}
		$sales = $this->Accountinfo->Sale->find('list');
		$this->set(compact('sales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Accountinfo->id = $id;
		if (!$this->Accountinfo->exists()) {
			throw new NotFoundException(__('Invalid accountinfo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Accountinfo->delete()) {
			$this->Flash->success(__('The accountinfo has been deleted.'));
		} else {
			$this->Flash->error(__('The accountinfo could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
