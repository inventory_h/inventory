<?php
App::uses('AppController', 'Controller');
/**
 * Paymenttypes Controller
 *
 * @property Paymenttype $Paymenttype
 * @property PaginatorComponent $Paginator
 */
class PaymenttypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Paymenttype->recursive = 0;
		$this->set('paymenttypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Paymenttype->exists($id)) {
			throw new NotFoundException(__('Invalid paymenttype'));
		}
		$options = array('conditions' => array('Paymenttype.' . $this->Paymenttype->primaryKey => $id));
		$this->set('paymenttype', $this->Paymenttype->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Paymenttype->create();
			if ($this->Paymenttype->save($this->request->data)) {
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The paymenttype could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Paymenttype->exists($id)) {
			throw new NotFoundException(__('Invalid paymenttype'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Paymenttype->save($this->request->data)) {
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The paymenttype could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Paymenttype.' . $this->Paymenttype->primaryKey => $id));
			$this->request->data = $this->Paymenttype->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Paymenttype->id = $id;
		if (!$this->Paymenttype->exists()) {
			throw new NotFoundException(__('Invalid paymenttype'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Paymenttype->delete()) {
			$this->Flash->success(__('The paymenttype has been deleted.'));
		} else {
			$this->Flash->error(__('The paymenttype could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
