<?php
/**
 * Purchase Fixture
 */
class PurchaseFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'invoicenumber' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'purchasedate' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'paymenttype_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'supplier_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'totalprice' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'invoicenumber' => 'Lorem ipsum dolor sit amet',
			'purchasedate' => '2016-05-05 16:42:35',
			'paymenttype_id' => 1,
			'supplier_id' => 1,
			'totalprice' => '',
			'created' => '2016-05-05 16:42:35',
			'modified' => '2016-05-05 16:42:35'
		),
	);

}
