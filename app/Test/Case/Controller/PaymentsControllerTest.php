<?php
App::uses('PaymentsController', 'Controller');

/**
 * PaymentsController Test Case
 */
class PaymentsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.payment',
		'app.supplier',
		'app.purchasereturn',
		'app.product',
		'app.category',
		'app.productmodel',
		'app.brand',
		'app.size',
		'app.purchasedetail',
		'app.purchase',
		'app.paymenttype',
		'app.salesdetail',
		'app.sale',
		'app.customer',
		'app.accountinfo',
		'app.unit',
		'app.salesreturn',
		'app.stock'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
