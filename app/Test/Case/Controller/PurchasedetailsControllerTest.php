<?php
App::uses('PurchasedetailsController', 'Controller');

/**
 * PurchasedetailsController Test Case
 */
class PurchasedetailsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.purchasedetail',
		'app.purchase',
		'app.paymenttype',
		'app.supplier',
		'app.payment',
		'app.purchasereturn',
		'app.product',
		'app.unit',
		'app.salesdetail',
		'app.sale',
		'app.customer',
		'app.accountinfo'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
