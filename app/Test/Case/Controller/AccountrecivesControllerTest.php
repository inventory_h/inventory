<?php
App::uses('AccountrecivesController', 'Controller');

/**
 * AccountrecivesController Test Case
 */
class AccountrecivesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.accountrecife',
		'app.customer',
		'app.sale',
		'app.accountinfo',
		'app.salesdetail',
		'app.product',
		'app.category',
		'app.brand',
		'app.productmodel',
		'app.size',
		'app.purchasedetail',
		'app.purchase',
		'app.paymenttype',
		'app.supplier',
		'app.payment',
		'app.purchasereturn',
		'app.unit',
		'app.salesreturn',
		'app.stock'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
