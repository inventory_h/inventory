<?php
App::uses('Paymentdetail', 'Model');

/**
 * Paymentdetail Test Case
 */
class PaymentdetailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.paymentdetail',
		'app.paymenttype',
		'app.purchase',
		'app.supplier',
		'app.payment',
		'app.purchasereturn',
		'app.product',
		'app.category',
		'app.brand',
		'app.productmodel',
		'app.size',
		'app.purchasedetail',
		'app.unit',
		'app.salesdetail',
		'app.sale',
		'app.customer',
		'app.accountrecife',
		'app.salesreturn',
		'app.accountinfo',
		'app.stock'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Paymentdetail = ClassRegistry::init('Paymentdetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Paymentdetail);

		parent::tearDown();
	}

}
