<?php
App::uses('Customer', 'Model');

/**
 * Customer Test Case
 */
class CustomerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.customer',
		'app.accountrecife',
		'app.sale',
		'app.accountinfo',
		'app.salesdetail',
		'app.product',
		'app.category',
		'app.productmodel',
		'app.brand',
		'app.size',
		'app.purchasedetail',
		'app.purchase',
		'app.paymenttype',
		'app.supplier',
		'app.payment',
		'app.purchasereturn',
		'app.unit',
		'app.salesreturn',
		'app.stock'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Customer = ClassRegistry::init('Customer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Customer);

		parent::tearDown();
	}

}
