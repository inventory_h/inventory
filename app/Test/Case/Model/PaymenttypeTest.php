<?php
App::uses('Paymenttype', 'Model');

/**
 * Paymenttype Test Case
 */
class PaymenttypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.paymenttype',
		'app.purchase',
		'app.supplier',
		'app.payment',
		'app.purchasereturn',
		'app.product',
		'app.category',
		'app.productmodel',
		'app.brand',
		'app.size',
		'app.purchasedetail',
		'app.unit',
		'app.salesdetail',
		'app.sale',
		'app.customer',
		'app.accountinfo',
		'app.salesreturn',
		'app.stock'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Paymenttype = ClassRegistry::init('Paymenttype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Paymenttype);

		parent::tearDown();
	}

}
