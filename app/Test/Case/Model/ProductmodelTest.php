<?php
App::uses('Productmodel', 'Model');

/**
 * Productmodel Test Case
 */
class ProductmodelTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.productmodel',
		'app.category',
		'app.product',
		'app.brand',
		'app.size',
		'app.purchasedetail',
		'app.purchase',
		'app.paymenttype',
		'app.supplier',
		'app.payment',
		'app.purchasereturn',
		'app.unit',
		'app.salesdetail',
		'app.sale',
		'app.customer',
		'app.accountinfo',
		'app.salesreturn',
		'app.stock'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Productmodel = ClassRegistry::init('Productmodel');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Productmodel);

		parent::tearDown();
	}

}
