<?php
App::uses('Salesreturn', 'Model');

/**
 * Salesreturn Test Case
 */
class SalesreturnTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.salesreturn',
		'app.product',
		'app.customer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Salesreturn = ClassRegistry::init('Salesreturn');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Salesreturn);

		parent::tearDown();
	}

}
