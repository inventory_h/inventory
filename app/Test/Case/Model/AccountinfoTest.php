<?php
App::uses('Accountinfo', 'Model');

/**
 * Accountinfo Test Case
 */
class AccountinfoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.accountinfo',
		'app.sale',
		'app.customer',
		'app.accountrecife',
		'app.salesreturn',
		'app.product',
		'app.category',
		'app.brand',
		'app.productmodel',
		'app.size',
		'app.purchasedetail',
		'app.purchase',
		'app.paymenttype',
		'app.salesdetail',
		'app.unit',
		'app.supplier',
		'app.payment',
		'app.purchasereturn',
		'app.stock'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Accountinfo = ClassRegistry::init('Accountinfo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Accountinfo);

		parent::tearDown();
	}

}
