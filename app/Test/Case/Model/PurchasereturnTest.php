<?php
App::uses('Purchasereturn', 'Model');

/**
 * Purchasereturn Test Case
 */
class PurchasereturnTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.purchasereturn',
		'app.product',
		'app.supplier',
		'app.payment',
		'app.purchase',
		'app.paymenttype',
		'app.purchasedetail'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Purchasereturn = ClassRegistry::init('Purchasereturn');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Purchasereturn);

		parent::tearDown();
	}

}
