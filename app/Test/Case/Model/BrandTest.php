<?php
App::uses('Brand', 'Model');

/**
 * Brand Test Case
 */
class BrandTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.brand',
		'app.category',
		'app.productmodel',
		'app.product',
		'app.size',
		'app.purchasedetail',
		'app.purchase',
		'app.paymenttype',
		'app.salesdetail',
		'app.sale',
		'app.customer',
		'app.accountrecife',
		'app.salesreturn',
		'app.accountinfo',
		'app.unit',
		'app.supplier',
		'app.payment',
		'app.purchasereturn',
		'app.stock'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Brand = ClassRegistry::init('Brand');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Brand);

		parent::tearDown();
	}

}
