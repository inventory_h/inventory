<?php
App::uses('Accountrecife', 'Model');

/**
 * Accountrecife Test Case
 */
class AccountrecifeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.accountrecife',
		'app.customer',
		'app.sale',
		'app.accountinfo',
		'app.salesdetail',
		'app.product',
		'app.category',
		'app.brand',
		'app.productmodel',
		'app.size',
		'app.purchasedetail',
		'app.purchase',
		'app.paymenttype',
		'app.supplier',
		'app.payment',
		'app.purchasereturn',
		'app.unit',
		'app.salesreturn',
		'app.stock'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Accountrecife = ClassRegistry::init('Accountrecife');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Accountrecife);

		parent::tearDown();
	}

}
