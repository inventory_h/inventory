<?php
App::uses('Salesdetail', 'Model');

/**
 * Salesdetail Test Case
 */
class SalesdetailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.salesdetail',
		'app.sale',
		'app.product',
		'app.unit',
		'app.purchasedetail',
		'app.paymenttype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Salesdetail = ClassRegistry::init('Salesdetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Salesdetail);

		parent::tearDown();
	}

}
