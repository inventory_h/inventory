<?php
App::uses('Payment', 'Model');

/**
 * Payment Test Case
 */
class PaymentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.payment',
		'app.supplier',
		'app.purchasereturn',
		'app.product',
		'app.category',
		'app.productmodel',
		'app.brand',
		'app.size',
		'app.purchasedetail',
		'app.purchase',
		'app.paymenttype',
		'app.salesdetail',
		'app.sale',
		'app.customer',
		'app.accountinfo',
		'app.unit',
		'app.salesreturn',
		'app.stock'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Payment = ClassRegistry::init('Payment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Payment);

		parent::tearDown();
	}

}
