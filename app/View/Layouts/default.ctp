<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
//$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo 'Welcome to Inventory management' ?>:
		<?php //echo $this->fetch('title'); ?>
	</title>
	
	 <!-- DataTables -->
	 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	 
	 <?php
	 	echo $this->Html->css(array('/plugins/datatables/jquery.dataTables.min','/plugins/datatables/buttons.bootstrap.min','/plugins/datatables/fixedHeader.bootstrap.min','/plugins/datatables/responsive.bootstrap.min','/plugins/datatables/scroller.bootstrap.min')); 
	 ?>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('bootstrap.min','core','components','icons','pages','menu','responsive','datepicker'));
		echo $this->Html->script(array('modernizr.min'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	
	<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-74137680-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body class="fixed-left">
	<div id="wrapper">
		<div class="topbar">
			<?php echo $this->element('header')?>
		</div>
		<div class="left side-menu">
			<?php echo $this->element('menu')?>
		</div>
		<div class="content-page">
			<div class="content">
				<div class="container">
					<?php echo $this->fetch('content'); ?>
				</div>
				
			</div>
			<footer class="footer text-right">
                    2016 © Adminto.
            </footer>
		</div>
		<?php echo $this->element('sql_dump'); ?>
	</div>
	<script>
            var resizefunc = [];
    </script>
    
    <!-- jQuery  -->
	<?php echo $this->Html->script(array('jquery.min',
		'bootstrap.min',
		'detect',
		'fastclick',
		'jquery.blockUI',
		'waves',
		'jquery.nicescroll',
		'jquery.slimscroll',
		'jquery.scrollTo.min',
		'/plugins/jquery-knob/jquery.knob',
		'/plugins/morris/morris.min',
		'/plugins/raphael/raphael-min',
		'/pages/jquery.dashboard',
		'bootstrap-datepicker',
		'jquery.core',
		'jquery.app'));
	?>
	
	<!-- Datatables-->
	
	<?php echo $this->Html->script(array('/plugins/datatables/jquery.dataTables.min',
		'/plugins/datatables/dataTables.bootstrap',
		'/plugins/datatables/dataTables.buttons.min',
		'/plugins/datatables/buttons.bootstrap.min',
		'/plugins/datatables/jszip.min',
		'/plugins/datatables/pdfmake.min',
		'/plugins/datatables/vfs_fonts',
		'/plugins/datatables/buttons.html5.min',
		'/plugins/datatables/buttons.print.min',
		'/plugins/datatables/dataTables.fixedHeader.min',
		'/plugins/datatables/dataTables.keyTable.min',
		'/plugins/datatables/dataTables.responsive.min',
		'/plugins/datatables/responsive.bootstrap.min',
		'/plugins/datatables/dataTables.scroller.min'
	));
	?>
	
	<script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
                $('#datatable-keytable').DataTable( { keys: true } );
                $('#datatable-responsive').DataTable();
                $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
            } );
            TableManageButtons.init();

        </script>
</body>
</html>
