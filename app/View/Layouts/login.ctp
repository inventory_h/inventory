<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<style>
#flashMessage {
  color: red;
  font-size: 16px;
  left: 40%;
  position: absolute;
  text-align: center;
  top: 16%;
}
@media only screen and (min-width: 320px) and (max-width: 768px) {    
    body{
        font-size: 18px;
    }
}
</style>
        <?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('bootstrap.min','uniform.default','bootstrap-responsive.min','style','style-responsive'));
		echo $this->Html->script(array('jquery-1.9.1.min','jquery-migrate-1.0.0.min','jquery-ui-1.10.0.custom.min','jquery.ui.touch-punch','modernizr','bootstrap.min','jquery.cookie','fullcalendar.min','jquery.dataTables.min','excanvas','jquery.flot','jquery.flot.pie','jquery.flot.stack','jquery.flot.resize.min','jquery.uniform.min'));

		
	?>
</head>
<body>


	<div id="message">
            <?php 
            if(!empty($this->params['pass'][0])){
                    echo $this->params['pass'][0];
            }

            ?>
        </div>
<?php echo $this->fetch('content'); ?>

</body>
</html>