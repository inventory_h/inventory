<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="header-title m-t-0 m-b-30" style="float: left;">Category</h4> 
            <?php echo $this->Html->link(__('<i class="ti-plus"></i>'), array('action' => 'add'),array('style'=>'float:right','class'=>'btn btn-success','escape'=>false)); ?>
			<table id="datatable-buttons" class="table table-striped table-bordered">
				  <thead>
					  <tr>
						   
							<th><?php echo 'Name'; ?></th>
							<th><?php echo 'Parent Category'; ?></th>
							
					  </tr>
				  </thead>   
				  <tbody>
					<?php foreach ($categories as $category): ?>
					<tr>
						<td><?php echo $category['Category']['name']; ?>&nbsp;</td>
						<td>
							<?php echo $this->Html->link($category['ParentCategory']['name'], array('controller' => 'categories', 'action' => 'view', $category['ParentCategory']['id'])); ?>
						</td>
						
						<td class="center">
							<?php echo $this->Html->link(__('<i class="ti-search "></i>'), array('action' => 'view', $category['Category']['id']),array('class'=>'','escape'=>false)); ?>
							<?php echo $this->Html->link(__('<i class="ti-pencil "></i>'), array('action' => 'edit', $category['Category']['id']),array('class'=>'','escape'=>false)); ?>
							<?php echo $this->Form->postLink(__('<i class="ti-trash"></i>'), array('action' => 'delete', $category['Category']['id']),array('class'=>'','escape'=>false), array('confirm' => __('Are you sure you want to delete # %s?'))); ?>
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
			</table>            
		</div>
	</div><!--/span-->
			
</div><!--/row-->




