
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
        <h4 class="header-title m-t-0 m-b-30">Edit Category</h4>
        <div class="row">
		<div class="col-lg-6">                
				<?php echo $this->Form->create('Category', array('class' => 'form-horizontal')); ?>
                     <?php
					echo $this->Form->input('id');
		
				?>
                    <div class="form-group">
	                    <label class="col-md-2 control-label">Name</label>
	                    <div class="col-md-10">
	                    	<?php echo $this->Form->input('name', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
	                    </div>
	                </div>
                    
<!--                    <div class="control-group">
                        <label class="control-label" for="date01">Parent Category</label>
                        <div class="controls">
                            <?php echo $this->Form->input('parent_id', array('options' => $parentCategories, 'empty' => 'Select Category', 'onChange'=>'getModel(this.value)','type' => 'select', 'label' => false, 'class' => 'span6 typeahead')); ?>
                        </div>
                    </div>-->
                    
					<div class="form-group">
	                    <label class="col-md-2 control-label"></label>
	                    <div class="col-md-10">
	                    	<button type="submit" class="btn btn-primary">Save</button>
	                    	<button type="reset" class="btn">Cancel</button>
	                    </div>
	                </div>
                <?php echo $this->Form->end(); ?> 

        	</div><!--/span-->
        	</div>
        </div>
	</div>
</div><!--/row-->
