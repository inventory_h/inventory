<!-- ========== Left Sidebar Start ========== -->

<div class="sidebar-inner slimscrollleft">

    <!-- User -->
    <div class="user-box">
        <div class="user-img">
            <?php echo $this->Html->link($this->Html->image('avatar.jpg', array('class' => 'img-circle img-thumbnail img-responsive', 'title' => 'Mat Helme')), array('controller' => 'users', 'action' => 'dashboard'), array('escape' => false)) ?>
        </div>
        <h5><a href="#">Mat Helme</a> </h5>
        <ul class="list-inline">
            <li>
                <a href="#" >
                    <i class="zmdi zmdi-settings"></i>
                </a>
            </li>

            <li>
                <a href="#" class="text-custom">
                    <i class="zmdi zmdi-power"></i>
                </a>
                <?php // echo $this->html-link('Logout', array('class' => 'text-custom zmdi zmdi-power','controller' => 'users', 'action' => 'logout'),array('escape' => false)) ?>
            </li>
        </ul>
    </div>
    <!-- End User -->

    <!--- Sidemenu -->
    <div id="sidebar-menu">
        <ul>
            <li class="text-muted menu-title">Navigation</li>

            <li>
                <?php echo $this->Html->link('<i class="zmdi zmdi-view-dashboard"></i><span> Dashboard</span> ', array('controller' => 'users', 'action' => 'dashboard'), array('class' => 'waves-effect active', 'escape' => false)) ?>
            </li>
            <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Master Entry </span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                    <li>
                        <?php echo $this->Html->link('Categories', array('controller' => 'categories', 'action' => 'index'), array('escape' => false)) ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link(' Models', array('controller' => 'productmodels', 'action' => 'index'), array('escape' => false)) ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link(' Brands', array('controller' => 'brands', 'action' => 'index'), array('escape' => false)) ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link(' Sizes', array('controller' => 'sizes', 'action' => 'index'), array('escape' => false)) ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link(' Units', array('controller' => 'units', 'action' => 'index'), array('escape' => false)) ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link(' Paymenttypes', array('controller' => 'paymenttypes', 'action' => 'index'), array('escape' => false)) ?>
                    </li>
                </ul>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="zmdi zmdi-view-list"></i><span> Customers</span> ', array('controller' => 'customers', 'action' => 'index'), array('class' => 'waves-effect', 'escape' => false)) ?>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="zmdi zmdi-view-list"></i><span> Products</span>', array('controller' => 'products', 'action' => 'index'), array('class' => 'waves-effect', 'escape' => false)) ?>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="zmdi zmdi-view-list"></i><span> Stock</span> ', array('controller' => 'stocks', 'action' => 'index'), array('class' => 'waves-effect', 'escape' => false)) ?>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="zmdi zmdi-view-list"></i><span> Suppliers</span></span>', array('controller' => 'suppliers', 'action' => 'index'), array('class' => 'waves-effect', 'escape' => false)) ?>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="zmdi zmdi-view-list"></i><span> Purchase</span>', array('controller' => 'purchases', 'action' => 'add'), array('class' => 'waves-effect', 'escape' => false)) ?>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="zmdi zmdi-view-list"></i><span> Sales</span>', array('controller' => 'sales', 'action' => 'index'), array('class' => 'waves-effect', 'escape' => false)) ?>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="zmdi zmdi-view-list"></i><span> Sales Return</span>', array('controller' => 'salesreturns', 'action' => 'index'), array('class' => 'waves-effect', 'escape' => false)) ?>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -->
    <div class="clearfix"></div>

</div>

<!-- Left Sidebar End -->

