<div class="accountinfos view">
<h2><?php echo __('Accountinfo'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($accountinfo['Accountinfo']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AccountNo'); ?></dt>
		<dd>
			<?php echo h($accountinfo['Accountinfo']['accountNo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AccountDate'); ?></dt>
		<dd>
			<?php echo h($accountinfo['Accountinfo']['accountDate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bankname'); ?></dt>
		<dd>
			<?php echo h($accountinfo['Accountinfo']['bankname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale'); ?></dt>
		<dd>
			<?php echo $this->Html->link($accountinfo['Sale']['id'], array('controller' => 'sales', 'action' => 'view', $accountinfo['Sale']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($accountinfo['Accountinfo']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($accountinfo['Accountinfo']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Accountinfo'), array('action' => 'edit', $accountinfo['Accountinfo']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Accountinfo'), array('action' => 'delete', $accountinfo['Accountinfo']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $accountinfo['Accountinfo']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Accountinfos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accountinfo'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sales'), array('controller' => 'sales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale'), array('controller' => 'sales', 'action' => 'add')); ?> </li>
	</ul>
</div>
