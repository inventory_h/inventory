<div class="accountinfos form">
<?php echo $this->Form->create('Accountinfo'); ?>
	<fieldset>
		<legend><?php echo __('Edit Accountinfo'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('accountNo');
		echo $this->Form->input('accountDate');
		echo $this->Form->input('bankname');
		echo $this->Form->input('sale_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Accountinfo.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Accountinfo.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Accountinfos'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sales'), array('controller' => 'sales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale'), array('controller' => 'sales', 'action' => 'add')); ?> </li>
	</ul>
</div>
