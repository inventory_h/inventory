<div class="purchases form">
<?php echo $this->Form->create('Purchase'); ?>
	<fieldset>
		<legend><?php echo __('Edit Purchase'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('invoicenumber');
		echo $this->Form->input('purchasedate');
		echo $this->Form->input('paymenttype_id');
		echo $this->Form->input('supplier_id');
		echo $this->Form->input('totalprice');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Purchase.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Purchase.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Purchases'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Paymenttypes'), array('controller' => 'paymenttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paymenttype'), array('controller' => 'paymenttypes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Suppliers'), array('controller' => 'suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Purchasedetails'), array('controller' => 'purchasedetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Purchasedetail'), array('controller' => 'purchasedetails', 'action' => 'add')); ?> </li>
	</ul>
</div>
