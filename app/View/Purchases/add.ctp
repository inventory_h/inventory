
<script>

    $(document).ready(function () {
        $("#PurchasePurchasedate").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

    var vr;
    function remove_file(id) {
        $('#total' + id).remove();
    }
    var i = 0;
    function addMulti_file() {
        appPrt = $(".append_attach_file_part").html().replace(/VR/g, i);
        $(".append_attach_file_here").append(appPrt);
        i++;

    }

</script>


<div class="append_attach_file_part col-md-12" style="display: none;">						
    <div class="col-md-12" id="totalVR" style="display: block;">	

        <div class="form-group">
            <label class="col-md-1">Name</label>
            <div class="col-md-2">
                <?php echo $this->Form->input('Purchasedetail.VR.product_id', array('options' => $products, 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
            </div>
            <label class="col-md-1">Unit</label>
            <div class="col-md-2">
                <?php echo $this->Form->input('Purchasedetail.VR.unit_id', array('options' => $units, 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
            </div>	               	                   	               

            <label class="col-md-1">Price</label>
            <div class="col-md-2">
                <?php echo $this->Form->input('Purchasedetail.VR.price', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
            </div>
            <label class="col-md-1">Qty</label>
            <div class="col-md-1">
                <?php echo $this->Form->input('Purchasedetail.VR.qty', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
            </div>
            <div class="col-md-1">
                <input type="button" class="btn btn-default" value="X" onclick="remove_file(VR)" />
            </div>
        </div>

    </div>	
</div>


<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="header-title m-t-0 m-b-30">Add Purchaset</h4>
            <div class="row">
                <div class="col-lg-12">                
                    <?php echo $this->Form->create('Purchase', array('class' => 'form-horizontal')); ?>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Inv. No.</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('invoicenumber', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                        <label class="col-md-2 control-label">Purchase Date</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('purchasedate', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Pyament</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('paymenttype_id', array('type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                        <label class="col-md-2 control-label">Supplier</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('supplier_id', array('type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-1">Name</label>
                        <div class="col-md-2">

                            <?php echo $this->Form->input('Purchasedetail.0.product_id', array('options' => $products, 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>

                        </div>
                        <label class="col-md-1">Unit</label>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('Purchasedetail.0.unit_id', array('options' => $units, 'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>

                        </div>

                        <label class="col-md-1">Price</label>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('Purchasedetail.0.price', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
                        </div>

                        <label class="col-md-1">Qty</label>
                        <div class="col-md-1">
                            <?php echo $this->Form->input('Purchasedetail.0.qty', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                        <input type="button" class="btn btn-default bucketbutton col-md-1" value="Add+" onclick="addMulti_file()" />
                    </div>


                    <div class="append_attach_file_here"></div>



                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn">Cancel</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?> 

                </div>




            </div>
        </div>
    </div>
</div>








