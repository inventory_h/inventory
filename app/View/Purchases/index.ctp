
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="header-title m-t-0 m-b-30" style="float: left;">Purchases</h4> 
            <?php echo $this->Html->link(__('<i class="ti-plus"></i>'),array('controller' => 'Purchases', 'action' => 'add'),array('style'=>'float:right','class'=>'btn btn-success','escape'=>false)); ?>
			<table id="datatable-buttons" class="table table-striped table-bordered">
				  <thead>
					  <tr>
						   
							<th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('invoicenumber'); ?></th>
                            <th><?php echo $this->Paginator->sort('purchasedate'); ?></th>
                            <th><?php echo $this->Paginator->sort('paymenttype_id'); ?></th>
                            <th><?php echo $this->Paginator->sort('supplier_id'); ?></th>
                            <th><?php echo $this->Paginator->sort('totalprice'); ?></th>
							
					  </tr>
				  </thead>   
				  <tbody>
				  
				   <?php foreach ($purchases as $purchase):

                       /* echo '<pre>';
                        print_r($purchase);
                        echo '</pre>';*/
                        
                        
                        ?>
                            <tr>
                                <td><?php echo h($purchase['Purchase']['id']); ?>&nbsp;</td>
                                <td><?php echo h($purchase['Purchase']['invoicenumber']); ?>&nbsp;</td>
                                <td><?php echo h($purchase['Purchase']['purchasedate']); ?>&nbsp;</td>
                                <td>
                                    <?php echo $this->Html->link($purchase['Paymenttype']['name'], array('controller' => 'paymenttypes', 'action' => 'view', $purchase['Paymenttype']['id'])); ?>
                                </td>
                                 <td><?php echo h($purchase['Supplier']['companyName']); ?>&nbsp;</td>
                                <td><?php echo h($purchase['purchase']['totalprice']); ?>&nbsp;</td>
                                
                                <td class="center">
                                <?php echo $this->Html->link(__('<i class="ti-search "></i>'),array('action' => 'view', $purchase['Purchase']['id']),array('class'=>'','escape'=>false)); ?>
                                <?php echo $this->Html->link(__('<i class="ti-pencil "></i>'), array('action' => 'edit', $purchase['Purchase']['id']),array('class'=>'','escape'=>false)); ?>
                                <?php echo $this->Form->postLink(__('<i class="ti-trash"></i>'), array('action' => 'delete', $purchase['Purchase']['id']),array('class'=>'','escape'=>false), array('confirm' => __('Are you sure you want to delete # %s?'))); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
				  
				  					 
					</tbody>
			</table>            
		</div>
	</div>
			
</div>















