





<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Forms</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Purchase Details</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <?php echo $this->Form->create('Purchasedetail', array('class' => 'form-horizontal')); ?>
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Purchase ID</label>
                        <div class="controls">
                            <?php echo $this->Form->input('purchase_id', array('options' => $purchases, 'empty' => 'Select purchase', 'type' => 'select', 'label' => false, 'class' => 'span6 typeahead')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">Product Name</label>
                        <div class="controls">
                            <?php echo $this->Form->input('product_id', array('options' => $products, 'empty' => 'Select Product', 'type' => 'select', 'label' => false, 'class' => 'span6 typeahead')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">Qty</label>
                        <div class="controls">
                            <?php echo $this->Form->input('qty', array('type'=>'text', 'label' => false, 'class' => 'span6 typeahead')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">Unit</label>
                        <div class="controls">
                            <?php echo $this->Form->input('unit_id', array('options' => $units, 'empty' => 'Select Unit', 'type' => 'select', 'label' => false, 'class' => 'span6 typeahead')); ?>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="date01">Price</label>
                        <div class="controls">
                            <?php echo $this->Form->input('price', array('type'=>'text','label' => false, 'class' => 'span6 typeahead')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">Discount</label>
                        <div class="controls">
                            <?php echo $this->Form->input('discount', array('type'=>'text', 'label' => false, 'class' => 'span6 typeahead')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">Vat</label>
                        <div class="controls">
                            <?php echo $this->Form->input('vat', array('type'=>'text', 'label' => false, 'class' => 'span6 typeahead')); ?>
                        </div>
                    </div>
                     

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
                <?php echo $this->Form->end(); ?> 

            </div>
        </div><!--/span-->

    </div><!--/row-->
</div>



