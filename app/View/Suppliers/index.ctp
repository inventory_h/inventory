	
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="header-title m-t-0 m-b-30" style="float: left;">Customers</h4> 
            <?php echo $this->Html->link(__('<i class="ti-plus"></i>'), array('controller'=>'suppliers','action' => 'add'),array('style'=>'float:right','class'=>'btn btn-success','escape'=>false)); ?>
			<table id="datatable-buttons" class="table table-striped table-bordered">
				  <thead>
					  <tr>
			   			 <th><?php echo $this->Paginator->sort('id'); ?></th>
								  <th><?php echo $this->Paginator->sort('Photo'); ?></th>
									<th><?php echo $this->Paginator->sort('companyName'); ?></th>
									<th><?php echo $this->Paginator->sort('phone'); ?></th>
									<th><?php echo $this->Paginator->sort('fax'); ?></th>
									<th><?php echo $this->Paginator->sort('address'); ?></th>
									<th><?php echo $this->Paginator->sort('Contact Person'); ?></th>
									<th><?php echo $this->Paginator->sort('Designation'); ?></th>
									
									
									<th class="actions"><?php echo __('Actions'); ?></th>
					  </tr>
				  </thead>   
				  <tbody>
				  <?php foreach ($suppliers as $supplier): ?>
								<tr>
									<td><?php echo h($supplier['Supplier']['id']); ?>&nbsp;</td>
									<td>
										<?php
							
											$check = WWW_ROOT."img/upload/supplier/" . $supplier['Supplier']['id'].'.png';
										
											if(file_exists($check)){
												echo $this->Html->image('upload/supplier/'.$supplier['Supplier']['id'].'.png',array('width'=>'64'));
											}else{
												echo $this->Html->image('no_img.jpg');
											}
										
										?>
									</td>
									<td><?php echo $supplier['Supplier']['companyName']; ?>&nbsp;</td>
									<td><?php echo h($supplier['Supplier']['phone']); ?>&nbsp;</td>
									<td><?php echo h($supplier['Supplier']['fax']); ?>&nbsp;</td>
									<td><?php echo h($supplier['Supplier']['address']); ?>&nbsp;</td>
									<td><?php echo $supplier['Supplier']['contactFname'].' '.$supplier['Supplier']['contactLname']; ?>&nbsp;</td>
									<td><?php echo h($supplier['Supplier']['contactTitle']); ?>&nbsp;</td>
									
									
									<td class="center">
										<?php echo $this->Html->link(__('<i class="ti-search "></i>'), array('action' => 'view', $supplier['Supplier']['id']),array('class'=>'','escape'=>false)); ?>
										<?php echo $this->Html->link(__('<i class="ti-pencil "></i>'), array('action' => 'edit', $supplier['Supplier']['id']),array('class'=>'','escape'=>false)); ?>
										<?php echo $this->Form->postLink(__('<i class="ti-trash"></i>'),array('action' => 'delete', $supplier['Supplier']['id']),array('class'=>'','escape'=>false), array('confirm' => __('Are you sure you want to delete # %s?'))); ?>
									</td>
								</tr>
							<?php endforeach; ?>
				  
					
					</tbody>
			</table>            
		</div>
	</div><!--/span-->
			
</div><!--/row-->	
	
	


