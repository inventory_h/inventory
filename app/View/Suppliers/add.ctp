

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
        <h4 class="header-title m-t-0 m-b-30">Add Supplier</h4>
        <div class="row">
		<div class="col-lg-7">                
				<?php echo $this->Form->create('Supplier',array('type'=>'file','class'=>'form-horizontal')); ?>
                    
                    <div class="form-group">
	                    <label class="col-md-3 control-label">Company Name</label>
	                    <div class="col-md-9">
	                    	<?php echo $this->Form->input('companyName',array('type'=>'text','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                </div>
	                
	                <div class="form-group">
	                    <label class="col-md-3 control-label">Phone</label>
	                    <div class="col-md-9">
	                    	<?php echo $this->Form->input('phone',array('type'=>'text','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                </div>
	                 <div class="form-group">
	                    <label class="col-md-3 control-label">Email</label>
	                    <div class="col-md-9">
	                    	 <?php echo $this->Form->input('email',array('type'=>'email','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                  </div>
	                 <div class="form-group">
	                    <label class="col-md-3 control-label">Fax</label>
	                    <div class="col-md-9">
	                    	 <?php echo $this->Form->input('fax',array('type'=>'text','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class="col-md-3 control-label">Address</label>
	                    <div class="col-md-9">
	                    	 <?php echo $this->Form->input('address',array('type'=>'text','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                </div>
	                
	                 <div class="form-group">
	                    <label class="col-md-3 control-label">Contact Person</label>
	                    <div class="col-md-4">
	                     <?php echo $this->Form->input('contactFname',array('type'=>'text','label'=>false,'class'=>'form-control','placeholder'=>'First Name'));?>
	                    </div>
	                    <div class="col-md-5">
	                     <?php echo $this->Form->input('contactLname',array('type'=>'text','label'=>false,'class'=>'form-control','placeholder'=>'Last Name'));?>
	                    </div>
	                </div>
	                 
	                 <div class="form-group">
	                    <label class="col-md-3 control-label">Designation</label>
	                    <div class="col-md-9">
	                    	 <?php echo $this->Form->input('contactTitle',array('type'=>'text','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                </div>
	                 
                   <div class="form-group">
                        <label class="col-md-3 control-label">Photo</label>
                        <div class="col-md-9">
                            <?php echo $this->Form->file('image', array('type'=>'file','label'=>false,'required'=>false)); ?>
                        </div>
                    </div>
                    
					<div class="form-group">
	                    <label class="col-md-3 control-label"></label>
	                    <div class="col-md-9">
	                    	<button type="submit" class="btn btn-primary">Save</button>
	                    	<button type="reset" class="btn">Cancel</button>
	                    </div>
	                </div>
                <?php echo $this->Form->end(); ?> 

        	</div><!--/span-->
        	</div>
        </div>
	</div>
</div><!--/row-->


