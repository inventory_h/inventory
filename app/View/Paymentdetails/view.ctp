<div class="paymentdetails view">
<h2><?php echo __('Paymentdetail'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($paymentdetail['Paymentdetail']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Paymenttype'); ?></dt>
		<dd>
			<?php echo $this->Html->link($paymentdetail['Paymenttype']['name'], array('controller' => 'paymenttypes', 'action' => 'view', $paymentdetail['Paymenttype']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale'); ?></dt>
		<dd>
			<?php echo $this->Html->link($paymentdetail['Sale']['id'], array('controller' => 'sales', 'action' => 'view', $paymentdetail['Sale']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($paymentdetail['Paymentdetail']['amount']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Paymentdetail'), array('action' => 'edit', $paymentdetail['Paymentdetail']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Paymentdetail'), array('action' => 'delete', $paymentdetail['Paymentdetail']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $paymentdetail['Paymentdetail']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Paymentdetails'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paymentdetail'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Paymenttypes'), array('controller' => 'paymenttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paymenttype'), array('controller' => 'paymenttypes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sales'), array('controller' => 'sales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale'), array('controller' => 'sales', 'action' => 'add')); ?> </li>
	</ul>
</div>
