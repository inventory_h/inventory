<div class="paymentdetails form">
<?php echo $this->Form->create('Paymentdetail'); ?>
	<fieldset>
		<legend><?php echo __('Edit Paymentdetail'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('paymenttype_id');
		echo $this->Form->input('sale_id');
		echo $this->Form->input('amount');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Paymentdetail.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Paymentdetail.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Paymentdetails'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Paymenttypes'), array('controller' => 'paymenttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paymenttype'), array('controller' => 'paymenttypes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sales'), array('controller' => 'sales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale'), array('controller' => 'sales', 'action' => 'add')); ?> </li>
	</ul>
</div>
