<div class="paymentdetails index">
	<h2><?php echo __('Paymentdetails'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('paymenttype_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_id'); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($paymentdetails as $paymentdetail): ?>
	<tr>
		<td><?php echo h($paymentdetail['Paymentdetail']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($paymentdetail['Paymenttype']['name'], array('controller' => 'paymenttypes', 'action' => 'view', $paymentdetail['Paymenttype']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($paymentdetail['Sale']['id'], array('controller' => 'sales', 'action' => 'view', $paymentdetail['Sale']['id'])); ?>
		</td>
		<td><?php echo h($paymentdetail['Paymentdetail']['amount']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $paymentdetail['Paymentdetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $paymentdetail['Paymentdetail']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $paymentdetail['Paymentdetail']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $paymentdetail['Paymentdetail']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Paymentdetail'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Paymenttypes'), array('controller' => 'paymenttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paymenttype'), array('controller' => 'paymenttypes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sales'), array('controller' => 'sales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale'), array('controller' => 'sales', 'action' => 'add')); ?> </li>
	</ul>
</div>
