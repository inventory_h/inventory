
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
        <h4 class="header-title m-t-0 m-b-30">Edit Stock</h4>
        <div class="row">
		<div class="col-lg-7">                
				<?php echo $this->Form->create('Stock',array('class'=>'form-horizontal')); ?>
                    <?php echo $this->Form->input('id');?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Product</label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('product_id', array('options' => $products, 'empty' => 'Select Product','type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group">
	                    <label class="col-md-3 control-label">Old Price</label>
	                    <div class="col-md-9">
	                    	<?php echo $this->Form->input('oldPrice', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
	                    </div>
	                </div>
                    <div class="form-group">
	                    <label class="col-md-3 control-label">Current Price</label>
	                    <div class="col-md-9">
	                    	<?php echo $this->Form->input('curretPrice', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class="col-md-3 control-label">Qty</label>
	                    <div class="col-md-9">
	                    	<?php echo $this->Form->input('stockqty', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
	                    </div>
	                </div>
	                <div class="form-group">
                        <label class="col-md-3 control-label">Unit</label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('unit_id', array('options' => $units, 'empty' => 'Select Unit','type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>
					<div class="form-group">
	                    <label class="col-md-3 control-label"></label>
	                    <div class="col-md-9">
	                    	<button type="submit" class="btn btn-primary">Save</button>
	                    	<button type="reset" class="btn">Cancel</button>
	                    </div>
	                </div>
                <?php echo $this->Form->end(); ?> 

        	</div><!--/span-->
        	</div>
        </div>
	</div>
</div><!--/row-->
