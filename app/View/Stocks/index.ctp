<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="header-title m-t-0 m-b-30" style="float: left;">Stock</h4> 
            <?php echo $this->Html->link(__('<i class="ti-plus"></i>'), array('action' => 'add'),array('style'=>'float:right','class'=>'btn btn-success','escape'=>false)); ?>
			<table id="datatable-buttons" class="table table-striped table-bordered">
				  <thead>
					  <tr>
		   				<th><?php echo $this->Paginator->sort('S/N.'); ?></th>
		   				<th><?php echo $this->Paginator->sort('product_id'); ?></th>
						<th><?php echo $this->Paginator->sort('stockqty'); ?></th>
						<th><?php echo $this->Paginator->sort('soldqty'); ?></th>						
						<th><?php echo $this->Paginator->sort('oldPrice'); ?></th>
						<th><?php echo $this->Paginator->sort('curretPrice'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					  </tr>
				  </thead>   
				  <tbody>
					<?php 
					$i=0;
					foreach ($stocks as $stock):
						$i++;
					
					?>
					<tr>
						<td><?php echo $i; ?>&nbsp;</td>
						<td>
							<?php echo $this->Html->link($stock['Product']['name'], array('controller' => 'products', 'action' => 'view', $stock['Product']['id'])); ?>
						</td>
						<td><?php echo h($stock['Stock']['stockqty']); ?>&nbsp;</td>
						<td><?php echo h($stock['Stock']['soldqty']); ?>&nbsp;</td>						
						<td><?php echo h($stock['Stock']['oldPrice']); ?>&nbsp;</td>
						<td><?php echo h($stock['Stock']['curretPrice']); ?>&nbsp;</td>
						<td class="center">
							<?php echo $this->Html->link(__('<i class="ti-search "></i>'), array('action' => 'view', $stock['Stock']['id']),array('class'=>'','escape'=>false)); ?>
							<?php echo $this->Html->link(__('<i class="ti-pencil "></i>'), array('action' => 'edit', $stock['Stock']['id']),array('class'=>'','escape'=>false)); ?>
							<?php echo $this->Form->postLink(__('<i class="ti-trash"></i>'), array('action' => 'delete', $stock['Stock']['id']),array('class'=>'','escape'=>false), array('confirm' => __('Are you sure you want to delete # %s?'))); ?>
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
			</table>            
		</div>
	</div><!--/span-->
			
</div><!--/row-->
