<div class="units view">
<h2><?php echo __('Unit'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($unit['Unit']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($unit['Unit']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($unit['Unit']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($unit['Unit']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Unit'), array('action' => 'edit', $unit['Unit']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Unit'), array('action' => 'delete', $unit['Unit']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $unit['Unit']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Units'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Unit'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Purchasedetails'), array('controller' => 'purchasedetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Purchasedetail'), array('controller' => 'purchasedetails', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Salesdetails'), array('controller' => 'salesdetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salesdetail'), array('controller' => 'salesdetails', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Purchasedetails'); ?></h3>
	<?php if (!empty($unit['Purchasedetail'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Purchase Id'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Vat'); ?></th>
		<th><?php echo __('Unit Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($unit['Purchasedetail'] as $purchasedetail): ?>
		<tr>
			<td><?php echo $purchasedetail['id']; ?></td>
			<td><?php echo $purchasedetail['purchase_id']; ?></td>
			<td><?php echo $purchasedetail['product_id']; ?></td>
			<td><?php echo $purchasedetail['qty']; ?></td>
			<td><?php echo $purchasedetail['price']; ?></td>
			<td><?php echo $purchasedetail['discount']; ?></td>
			<td><?php echo $purchasedetail['vat']; ?></td>
			<td><?php echo $purchasedetail['unit_id']; ?></td>
			<td><?php echo $purchasedetail['created']; ?></td>
			<td><?php echo $purchasedetail['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'purchasedetails', 'action' => 'view', $purchasedetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'purchasedetails', 'action' => 'edit', $purchasedetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'purchasedetails', 'action' => 'delete', $purchasedetail['id']), array('confirm' => __('Are you sure you want to delete # %s?', $purchasedetail['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Purchasedetail'), array('controller' => 'purchasedetails', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Salesdetails'); ?></h3>
	<?php if (!empty($unit['Salesdetail'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sale Id'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Vat'); ?></th>
		<th><?php echo __('Unit Id'); ?></th>
		<th><?php echo __('Paymenttype Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($unit['Salesdetail'] as $salesdetail): ?>
		<tr>
			<td><?php echo $salesdetail['id']; ?></td>
			<td><?php echo $salesdetail['sale_id']; ?></td>
			<td><?php echo $salesdetail['product_id']; ?></td>
			<td><?php echo $salesdetail['qty']; ?></td>
			<td><?php echo $salesdetail['price']; ?></td>
			<td><?php echo $salesdetail['discount']; ?></td>
			<td><?php echo $salesdetail['vat']; ?></td>
			<td><?php echo $salesdetail['unit_id']; ?></td>
			<td><?php echo $salesdetail['paymenttype_id']; ?></td>
			<td><?php echo $salesdetail['created']; ?></td>
			<td><?php echo $salesdetail['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'salesdetails', 'action' => 'view', $salesdetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'salesdetails', 'action' => 'edit', $salesdetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'salesdetails', 'action' => 'delete', $salesdetail['id']), array('confirm' => __('Are you sure you want to delete # %s?', $salesdetail['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Salesdetail'), array('controller' => 'salesdetails', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
