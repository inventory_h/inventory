<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="header-title m-t-0 m-b-30" style="float: left;">Units</h4> 
            <?php echo $this->Html->link(__('<i class="ti-plus"></i>'), array('action' => 'add'),array('style'=>'float:right','class'=>'btn btn-success','escape'=>false)); ?>
			<table id="datatable-buttons" class="table table-striped table-bordered">
				  <thead>
					  <tr>
			  				<th><?php echo $this->Paginator->sort('id'); ?></th>
							<th><?php echo $this->Paginator->sort('name'); ?></th>
							<th><?php echo $this->Paginator->sort('created'); ?></th>
							<th><?php echo $this->Paginator->sort('modified'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
					  </tr>
				  </thead>   
				  <tbody>
					<?php foreach ($units as $unit): ?>
					<tr>
						<td><?php echo h($unit['Unit']['id']); ?>&nbsp;</td>
						<td><?php echo h($unit['Unit']['name']); ?>&nbsp;</td>
						<td><?php echo h($unit['Unit']['created']); ?>&nbsp;</td>
						<td><?php echo h($unit['Unit']['modified']); ?>&nbsp;</td>
						
						<td class="center">
							<?php echo $this->Html->link(__('<i class="ti-search "></i>'), array('action' => 'view', $unit['Unit']['id']),array('class'=>'','escape'=>false)); ?>
							<?php echo $this->Html->link(__('<i class="ti-pencil "></i>'), array('action' => 'edit', $unit['Unit']['id']),array('class'=>'','escape'=>false)); ?>
							<?php echo $this->Form->postLink(__('<i class="ti-trash"></i>'), array('action' => 'delete', $unit['Unit']['id']),array('class'=>'','escape'=>false), array('confirm' => __('Are you sure you want to delete # %s?'))); ?>
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
			</table>            
		</div>
	</div><!--/span-->
			
</div><!--/row-->
