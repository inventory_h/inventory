
<script>
var path='<?php echo $this->webroot;?>';
function getBrand(id){
	$.ajax({
		type: 'POST',
		url: path +'brands/dropdown',
		data: {category_id:id},
		success: function(data){
			
		$("#ProductBrandId").html(data);
		
		}
	});
}
	function getModel(id){
		$.ajax({
			type: 'POST',
			url: path +'productmodels/dropdown2',
			data: {brand_id:id},
			success: function(data){
				
			$("#ProductProductmodelId").html(data);
			
			}
		});
	}

	function getSize(id){
		$.ajax({
			type: 'POST',
			url: path +'sizes/dropdown3',
			data: {productmodel_id:id},
			success: function(data){
				
			$("#ProductSizeId").html(data);
			
			}
		});
	}
</script>


<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
        <h4 class="header-title m-t-0 m-b-30">Add Product</h4>
        <div class="row">
		<div class="col-lg-7">                
				<?php echo $this->Form->create('Product', array('class' => 'form-horizontal')); ?>
                    
                    <div class="form-group">
	                    <label class="col-md-3 control-label">Name</label>
	                    <div class="col-md-9">
	                    	<?php echo $this->Form->input('name', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
	                    </div>
	                </div>
                    
                   <div class="form-group">
                        <label class="col-md-3 control-label">Category</label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('category_id', array('options' =>$categories, 'empty' => 'Select Category', 'onChange'=>'getBrand(this.value)','type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Brand</label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('brand_id', array('options' => $brands, 'empty' => 'Select Brand', 'onChange'=>'getModel(this.value)','type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Model</label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('productmodel_id', array('options' => $productmodels, 'empty' => 'Select Model', 'onChange'=>'getSize(this.value)','type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Size & Color</label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('size_id', array('options' => $sizes, 'empty' => 'Select Size & Color','type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Description</label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('description', array('type' => 'textaria','rows' => 3, 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>
                    
					<div class="form-group">
	                    <label class="col-md-3 control-label"></label>
	                    <div class="col-md-9">
	                    	<button type="submit" class="btn btn-primary">Save</button>
	                    	<button type="reset" class="btn">Cancel</button>
	                    </div>
	                </div>
                <?php echo $this->Form->end(); ?> 

        	</div>
        	</div>
        </div>
	</div>
</div>
