<div class="products view">
<h2><?php echo __('Product'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($product['Product']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($product['Product']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($product['Category']['name'], array('controller' => 'categories', 'action' => 'view', $product['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Productmodel'); ?></dt>
		<dd>
			<?php echo $this->Html->link($product['Productmodel']['name'], array('controller' => 'productmodels', 'action' => 'view', $product['Productmodel']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Brand'); ?></dt>
		<dd>
			<?php echo $this->Html->link($product['Brand']['name'], array('controller' => 'brands', 'action' => 'view', $product['Brand']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($product['Product']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Size'); ?></dt>
		<dd>
			<?php echo $this->Html->link($product['Size']['name'], array('controller' => 'sizes', 'action' => 'view', $product['Size']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($product['Product']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($product['Product']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Product'), array('action' => 'edit', $product['Product']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Product'), array('action' => 'delete', $product['Product']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $product['Product']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Productmodels'), array('controller' => 'productmodels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Productmodel'), array('controller' => 'productmodels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Brands'), array('controller' => 'brands', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Brand'), array('controller' => 'brands', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sizes'), array('controller' => 'sizes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Size'), array('controller' => 'sizes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Purchasedetails'), array('controller' => 'purchasedetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Purchasedetail'), array('controller' => 'purchasedetails', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Purchasereturns'), array('controller' => 'purchasereturns', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Purchasereturn'), array('controller' => 'purchasereturns', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Salesdetails'), array('controller' => 'salesdetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salesdetail'), array('controller' => 'salesdetails', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Salesreturns'), array('controller' => 'salesreturns', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salesreturn'), array('controller' => 'salesreturns', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stocks'), array('controller' => 'stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stock'), array('controller' => 'stocks', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Purchasedetails'); ?></h3>
	<?php if (!empty($product['Purchasedetail'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Purchase Id'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Vat'); ?></th>
		<th><?php echo __('Unit Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($product['Purchasedetail'] as $purchasedetail): ?>
		<tr>
			<td><?php echo $purchasedetail['id']; ?></td>
			<td><?php echo $purchasedetail['purchase_id']; ?></td>
			<td><?php echo $purchasedetail['product_id']; ?></td>
			<td><?php echo $purchasedetail['qty']; ?></td>
			<td><?php echo $purchasedetail['price']; ?></td>
			<td><?php echo $purchasedetail['discount']; ?></td>
			<td><?php echo $purchasedetail['vat']; ?></td>
			<td><?php echo $purchasedetail['unit_id']; ?></td>
			<td><?php echo $purchasedetail['created']; ?></td>
			<td><?php echo $purchasedetail['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'purchasedetails', 'action' => 'view', $purchasedetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'purchasedetails', 'action' => 'edit', $purchasedetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'purchasedetails', 'action' => 'delete', $purchasedetail['id']), array('confirm' => __('Are you sure you want to delete # %s?', $purchasedetail['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Purchasedetail'), array('controller' => 'purchasedetails', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Purchasereturns'); ?></h3>
	<?php if (!empty($product['Purchasereturn'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Returndate'); ?></th>
		<th><?php echo __('Supplier Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($product['Purchasereturn'] as $purchasereturn): ?>
		<tr>
			<td><?php echo $purchasereturn['id']; ?></td>
			<td><?php echo $purchasereturn['product_id']; ?></td>
			<td><?php echo $purchasereturn['qty']; ?></td>
			<td><?php echo $purchasereturn['price']; ?></td>
			<td><?php echo $purchasereturn['returndate']; ?></td>
			<td><?php echo $purchasereturn['supplier_id']; ?></td>
			<td><?php echo $purchasereturn['created']; ?></td>
			<td><?php echo $purchasereturn['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'purchasereturns', 'action' => 'view', $purchasereturn['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'purchasereturns', 'action' => 'edit', $purchasereturn['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'purchasereturns', 'action' => 'delete', $purchasereturn['id']), array('confirm' => __('Are you sure you want to delete # %s?', $purchasereturn['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Purchasereturn'), array('controller' => 'purchasereturns', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Salesdetails'); ?></h3>
	<?php if (!empty($product['Salesdetail'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sale Id'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Vat'); ?></th>
		<th><?php echo __('Unit Id'); ?></th>
		<th><?php echo __('Paymenttype Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($product['Salesdetail'] as $salesdetail): ?>
		<tr>
			<td><?php echo $salesdetail['id']; ?></td>
			<td><?php echo $salesdetail['sale_id']; ?></td>
			<td><?php echo $salesdetail['product_id']; ?></td>
			<td><?php echo $salesdetail['qty']; ?></td>
			<td><?php echo $salesdetail['price']; ?></td>
			<td><?php echo $salesdetail['discount']; ?></td>
			<td><?php echo $salesdetail['vat']; ?></td>
			<td><?php echo $salesdetail['unit_id']; ?></td>
			<td><?php echo $salesdetail['paymenttype_id']; ?></td>
			<td><?php echo $salesdetail['created']; ?></td>
			<td><?php echo $salesdetail['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'salesdetails', 'action' => 'view', $salesdetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'salesdetails', 'action' => 'edit', $salesdetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'salesdetails', 'action' => 'delete', $salesdetail['id']), array('confirm' => __('Are you sure you want to delete # %s?', $salesdetail['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Salesdetail'), array('controller' => 'salesdetails', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Salesreturns'); ?></h3>
	<?php if (!empty($product['Salesreturn'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Returndate'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($product['Salesreturn'] as $salesreturn): ?>
		<tr>
			<td><?php echo $salesreturn['id']; ?></td>
			<td><?php echo $salesreturn['product_id']; ?></td>
			<td><?php echo $salesreturn['qty']; ?></td>
			<td><?php echo $salesreturn['price']; ?></td>
			<td><?php echo $salesreturn['customer_id']; ?></td>
			<td><?php echo $salesreturn['returndate']; ?></td>
			<td><?php echo $salesreturn['created']; ?></td>
			<td><?php echo $salesreturn['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'salesreturns', 'action' => 'view', $salesreturn['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'salesreturns', 'action' => 'edit', $salesreturn['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'salesreturns', 'action' => 'delete', $salesreturn['id']), array('confirm' => __('Are you sure you want to delete # %s?', $salesreturn['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Salesreturn'), array('controller' => 'salesreturns', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Stocks'); ?></h3>
	<?php if (!empty($product['Stock'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Stockqty'); ?></th>
		<th><?php echo __('Soldqty'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('OldPrice'); ?></th>
		<th><?php echo __('CurretPrice'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($product['Stock'] as $stock): ?>
		<tr>
			<td><?php echo $stock['id']; ?></td>
			<td><?php echo $stock['stockqty']; ?></td>
			<td><?php echo $stock['soldqty']; ?></td>
			<td><?php echo $stock['product_id']; ?></td>
			<td><?php echo $stock['oldPrice']; ?></td>
			<td><?php echo $stock['curretPrice']; ?></td>
			<td><?php echo $stock['created']; ?></td>
			<td><?php echo $stock['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'stocks', 'action' => 'view', $stock['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'stocks', 'action' => 'edit', $stock['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'stocks', 'action' => 'delete', $stock['id']), array('confirm' => __('Are you sure you want to delete # %s?', $stock['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Stock'), array('controller' => 'stocks', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
