<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="header-title m-t-0 m-b-30" style="float: left;">Brand</h4> 
            <?php echo $this->Html->link(__('<i class="ti-plus"></i>'), array('action' => 'add'),array('style'=>'float:right','class'=>'btn btn-success','escape'=>false)); ?>
			<table id="datatable-buttons" class="table table-striped table-bordered">
				  <thead>
					  <tr>
				 			<th><?php echo $this->Paginator->sort('id'); ?></th>
							<th><?php echo $this->Paginator->sort('name'); ?></th>
							<th><?php echo $this->Paginator->sort('category_id'); ?></th>
							<th><?php echo $this->Paginator->sort('brand_id'); ?></th>
							<th><?php echo $this->Paginator->sort('Model'); ?></th>							
							<th><?php echo $this->Paginator->sort('description'); ?></th>
							<th><?php echo $this->Paginator->sort('Size & Color'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
					  </tr>
				  </thead>   
				  <tbody>
				  
					<?php 
					$i=0;
					
					foreach ($products as $product):
						$i++;
					?>
					
					<tr>
						<td><?php echo $i; ?>&nbsp;</td>
						<td><?php echo h($product['Product']['name']); ?>&nbsp;</td>
						<td>
							<?php echo $this->Html->link($product['Category']['name'], array('controller' => 'categories', 'action' => 'view', $product['Category']['id'])); ?>
						</td>
						<td>
							<?php echo $this->Html->link($product['Brand']['name'], array('controller' => 'brands', 'action' => 'view', $product['Brand']['id'])); ?>
						</td>
						<td>
							<?php echo $this->Html->link($product['Productmodel']['name'], array('controller' => 'productmodels', 'action' => 'view', $product['Productmodel']['id'])); ?>
						</td>
						
						<td><?php echo h($product['Product']['description']); ?>&nbsp;</td>
						<td>
							<?php echo $this->Html->link($product['Size']['name'], array('controller' => 'sizes', 'action' => 'view', $product['Size']['id'])); ?>
						</td>
						<td class="center">
							<?php echo $this->Html->link(__('<i class="ti-search "></i>'), array('action' => 'view', $product['Product']['id']),array('class'=>'','escape'=>false)); ?>
							<?php echo $this->Html->link(__('<i class="ti-pencil "></i>'), array('action' => 'edit', $product['Product']['id']),array('class'=>'','escape'=>false)); ?>
							<?php echo $this->Form->postLink(__('<i class="ti-trash"></i>'), array('action' => 'delete', $product['Product']['id']),array('class'=>'','escape'=>false), array('confirm' => __('Are you sure you want to delete # %s?'))); ?>
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
			</table>            
		</div>
	</div><!--/span-->
			
</div><!--/row-->
