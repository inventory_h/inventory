
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Forms</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Customer View</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <?php echo $this->Form->create('Customer', array('class' => 'form-horizontal')); ?>
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">ID</label>
                        <div class="controls">
                            <?php echo h($customer['Customer']['id']); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">Name</label>
                        <div class="controls">
                            <?php echo h($customer['Customer']['name']); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Mobile</label>
                        <div class="controls">
                            <?php echo h($customer['Customer']['mobile']); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Created</label>
                        <div class="controls">
                            <?php echo h($customer['Customer']['created']); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Modified</label>
                        <div class="controls">
                            <?php echo h($customer['Customer']['modified']); ?>
                        </div>
                    </div>
                </fieldset>
                <?php echo $this->Form->end(); ?> 

            </div>
        </div><!--/span-->

    </div><!--/row-->
</div>





