<div class="salesdetails view">
<h2><?php echo __('Salesdetail'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($salesdetail['Salesdetail']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale'); ?></dt>
		<dd>
			<?php echo $this->Html->link($salesdetail['Sale']['id'], array('controller' => 'sales', 'action' => 'view', $salesdetail['Sale']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($salesdetail['Product']['name'], array('controller' => 'products', 'action' => 'view', $salesdetail['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Qty'); ?></dt>
		<dd>
			<?php echo h($salesdetail['Salesdetail']['qty']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($salesdetail['Salesdetail']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Discount'); ?></dt>
		<dd>
			<?php echo h($salesdetail['Salesdetail']['discount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vat'); ?></dt>
		<dd>
			<?php echo h($salesdetail['Salesdetail']['vat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($salesdetail['Unit']['name'], array('controller' => 'units', 'action' => 'view', $salesdetail['Unit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Paymenttype'); ?></dt>
		<dd>
			<?php echo $this->Html->link($salesdetail['Paymenttype']['name'], array('controller' => 'paymenttypes', 'action' => 'view', $salesdetail['Paymenttype']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($salesdetail['Salesdetail']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($salesdetail['Salesdetail']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Salesdetail'), array('action' => 'edit', $salesdetail['Salesdetail']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Salesdetail'), array('action' => 'delete', $salesdetail['Salesdetail']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $salesdetail['Salesdetail']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Salesdetails'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salesdetail'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sales'), array('controller' => 'sales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale'), array('controller' => 'sales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Units'), array('controller' => 'units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Unit'), array('controller' => 'units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Paymenttypes'), array('controller' => 'paymenttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paymenttype'), array('controller' => 'paymenttypes', 'action' => 'add')); ?> </li>
	</ul>
</div>
