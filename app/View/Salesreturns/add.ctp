
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
        <h4 class="header-title m-t-0 m-b-30">Add Salesreturn</h4>
        <div class="row">
		<div class="col-lg-7">                
				<?php echo $this->Form->create('Salesreturn', array('class' => 'form-horizontal')); ?>
                    
                    <div class="form-group">
	                    <label class="col-md-3 control-label">Customer</label>
	                    <div class="col-md-9">
	                    	<?php echo $this->Form->input('customer_id',array('id'=>'selectError','data-rel'=>'chosen','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                </div>
	                
	                 <div class="form-group">
	                    <label class="col-md-3 control-label">Name</label>
	                    <div class="col-md-9">
	                    	<?php echo $this->Form->input('product_id',array('id'=>'selectError1','data-rel'=>'chosen','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                </div>
	                 <div class="form-group">
	                    <label class="col-md-3 control-label">Qty</label>
	                    <div class="col-md-9">
	                    	<?php echo $this->Form->input('qty',array('type'=>'text','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                </div>
	                 <div class="form-group">
	                    <label class="col-md-3 control-label">Price</label>
	                    <div class="col-md-9">
	                    	<?php echo $this->Form->input('price',array('type'=>'text','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                </div>
	                 <div class="form-group">
	                    <label class="col-md-3 control-label">Return Date</label>
	                    <div class="col-md-9">
	                    	 <?php echo $this->Form->input('returndate',array('type'=>'text','label'=>false,'class'=>'form-control'));?>
	                    </div>
	                </div>
                    			                    
					<div class="form-group">
	                    <label class="col-md-3 control-label"></label>
	                    <div class="col-md-9">
	                    	<button type="submit" class="btn btn-primary">Save</button>
	                    	<button type="reset" class="btn">Cancel</button>
	                    </div>
	                </div>
                <?php echo $this->Form->end(); ?> 

        	</div><!--/span-->
        	</div>
        </div>
	</div>
</div><!--/row-->


