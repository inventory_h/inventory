	
	<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="header-title m-t-0 m-b-30" style="float: left;">Salesreturns</h4> 
            <?php echo $this->Html->link(__('<i class="ti-plus"></i>'), array('controller'=>'salesreturns','action' => 'add'),array('style'=>'float:right','class'=>'btn btn-success','escape'=>false)); ?>
			<table id="datatable-buttons" class="table table-striped table-bordered">
				  <thead>
					  <tr>
			   			 		<th><?php echo $this->Paginator->sort('id'); ?></th>
									<th><?php echo $this->Paginator->sort('product_id'); ?></th>
									<th><?php echo $this->Paginator->sort('qty'); ?></th>
									<th><?php echo $this->Paginator->sort('price'); ?></th>
									<th><?php echo $this->Paginator->sort('customer_id'); ?></th>
									<th><?php echo $this->Paginator->sort('returndate'); ?></th>
									<th><?php echo $this->Paginator->sort('created'); ?></th>
									<th><?php echo $this->Paginator->sort('modified'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
					  </tr>
				  </thead>   
				  <tbody>
				  
				  <?php foreach ($salesreturns as $salesreturn): ?>
								<tr>
									<td><?php echo h($salesreturn['Salesreturn']['id']); ?>&nbsp;</td>
									<td>
										<?php echo $this->Html->link($salesreturn['Product']['name'], array('controller' => 'products', 'action' => 'view', $salesreturn['Product']['id'])); ?>
									</td>
									<td><?php echo h($salesreturn['Salesreturn']['qty']); ?>&nbsp;</td>
									<td><?php echo h($salesreturn['Salesreturn']['price']); ?>&nbsp;</td>
									<td>
										<?php echo $this->Html->link($salesreturn['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $salesreturn['Customer']['id'])); ?>
									</td>
									<td><?php echo h($salesreturn['Salesreturn']['returndate']); ?>&nbsp;</td>
									<td><?php echo h($salesreturn['Salesreturn']['created']); ?>&nbsp;</td>
									<td><?php echo h($salesreturn['Salesreturn']['modified']); ?>&nbsp;</td>
									
									<td class="center">
										<?php echo $this->Html->link(__('<i class="ti-search "></i>'), array('action' => 'view', $salesreturn['Salesreturn']['id']),array('class'=>'','escape'=>false)); ?>
										<?php echo $this->Html->link(__('<i class="ti-pencil "></i>'),array('action' => 'edit', $salesreturn['Salesreturn']['id']),array('class'=>'','escape'=>false)); ?>
										<?php echo $this->Form->postLink(__('<i class="ti-trash"></i>'),array('action' => 'delete', $salesreturn['Salesreturn']['id']),array('class'=>'','escape'=>false), array('confirm' => __('Are you sure you want to delete # %s?'))); ?>
									</td>
									
									
								</tr>
							<?php endforeach; ?>
				 
				  
					
					</tbody>
			</table>            
		</div>
	</div><!--/span-->
			
</div><!--/row-->	
	
	
	
	
	
