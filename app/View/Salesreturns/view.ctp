<div class="salesreturns view">
<h2><?php echo __('Salesreturn'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($salesreturn['Salesreturn']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($salesreturn['Product']['name'], array('controller' => 'products', 'action' => 'view', $salesreturn['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Qty'); ?></dt>
		<dd>
			<?php echo h($salesreturn['Salesreturn']['qty']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($salesreturn['Salesreturn']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($salesreturn['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $salesreturn['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Returndate'); ?></dt>
		<dd>
			<?php echo h($salesreturn['Salesreturn']['returndate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($salesreturn['Salesreturn']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($salesreturn['Salesreturn']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Salesreturn'), array('action' => 'edit', $salesreturn['Salesreturn']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Salesreturn'), array('action' => 'delete', $salesreturn['Salesreturn']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $salesreturn['Salesreturn']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Salesreturns'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salesreturn'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>
