<div class="purchasereturns view">
<h2><?php echo __('Purchasereturn'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($purchasereturn['Purchasereturn']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($purchasereturn['Product']['name'], array('controller' => 'products', 'action' => 'view', $purchasereturn['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Qty'); ?></dt>
		<dd>
			<?php echo h($purchasereturn['Purchasereturn']['qty']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($purchasereturn['Purchasereturn']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Returndate'); ?></dt>
		<dd>
			<?php echo h($purchasereturn['Purchasereturn']['returndate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Supplier'); ?></dt>
		<dd>
			<?php echo $this->Html->link($purchasereturn['Supplier']['id'], array('controller' => 'suppliers', 'action' => 'view', $purchasereturn['Supplier']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($purchasereturn['Purchasereturn']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($purchasereturn['Purchasereturn']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Purchasereturn'), array('action' => 'edit', $purchasereturn['Purchasereturn']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Purchasereturn'), array('action' => 'delete', $purchasereturn['Purchasereturn']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $purchasereturn['Purchasereturn']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Purchasereturns'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Purchasereturn'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Suppliers'), array('controller' => 'suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
