<div class="purchasereturns index">
	<h2><?php echo __('Purchasereturns'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('product_id'); ?></th>
			<th><?php echo $this->Paginator->sort('qty'); ?></th>
			<th><?php echo $this->Paginator->sort('price'); ?></th>
			<th><?php echo $this->Paginator->sort('returndate'); ?></th>
			<th><?php echo $this->Paginator->sort('supplier_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($purchasereturns as $purchasereturn): ?>
	<tr>
		<td><?php echo h($purchasereturn['Purchasereturn']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($purchasereturn['Product']['name'], array('controller' => 'products', 'action' => 'view', $purchasereturn['Product']['id'])); ?>
		</td>
		<td><?php echo h($purchasereturn['Purchasereturn']['qty']); ?>&nbsp;</td>
		<td><?php echo h($purchasereturn['Purchasereturn']['price']); ?>&nbsp;</td>
		<td><?php echo h($purchasereturn['Purchasereturn']['returndate']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($purchasereturn['Supplier']['id'], array('controller' => 'suppliers', 'action' => 'view', $purchasereturn['Supplier']['id'])); ?>
		</td>
		<td><?php echo h($purchasereturn['Purchasereturn']['created']); ?>&nbsp;</td>
		<td><?php echo h($purchasereturn['Purchasereturn']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $purchasereturn['Purchasereturn']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $purchasereturn['Purchasereturn']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $purchasereturn['Purchasereturn']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $purchasereturn['Purchasereturn']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Purchasereturn'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Suppliers'), array('controller' => 'suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
