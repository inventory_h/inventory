<div class="sales form">
<?php echo $this->Form->create('Sale'); ?>
	<fieldset>
		<legend><?php echo __('Edit Sale'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('invoicenumber');
		echo $this->Form->input('salesdate');
		echo $this->Form->input('customer_id');
		echo $this->Form->input('totalprice');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Sale.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Sale.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Sales'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accountinfos'), array('controller' => 'accountinfos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accountinfo'), array('controller' => 'accountinfos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Salesdetails'), array('controller' => 'salesdetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salesdetail'), array('controller' => 'salesdetails', 'action' => 'add')); ?> </li>
	</ul>
</div>
