
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="header-title m-t-0 m-b-30" style="float: left;">Sales</h4> 
            <?php echo $this->Html->link(__('<i class="ti-plus"></i>'), array('controller' => 'sales', 'action' => 'add'),array('style'=>'float:right','class'=>'btn btn-success','escape'=>false)); ?>
			<table id="datatable-buttons" class="table table-striped table-bordered">
				  <thead>
					  <tr>
						   
							<th><?php echo $this->Paginator->sort('id'); ?></th>
                            <th><?php echo $this->Paginator->sort('invoicenumber'); ?></th>
                            <th><?php echo $this->Paginator->sort('salesdate'); ?></th>
                            <th><?php echo $this->Paginator->sort('customer_id'); ?></th>
                            <th><?php echo $this->Paginator->sort('totalprice'); ?></th>
                           
							
					  </tr>
				  </thead>   
				  <tbody>
				  
				  <?php foreach ($sales as $sale): ?>
                            <tr>
                                <td><?php echo h($sale['Sale']['id']); ?>&nbsp;</td>
                                <td><?php echo h($sale['Sale']['invoicenumber']); ?>&nbsp;</td>
                                <td><?php echo h($sale['Sale']['salesdate']); ?>&nbsp;</td>
                                <td>
                                    <?php echo $this->Html->link($sale['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $sale['Customer']['id'])); ?>
                                </td>
                                <td><?php echo h($sale['Sale']['totalprice']); ?>&nbsp;</td>
                               
                                <td class="center">
                                 <?php echo $this->Html->link(__('<i class="ti-search "></i>'),array('action' => 'view', $sale['Sale']['id']),array('class'=>'','escape'=>false)); ?>
                                <?php echo $this->Html->link(__('<i class="ti-pencil "></i>'),array('action' => 'edit', $sale['Sale']['id']),array('class'=>'','escape'=>false)); ?>
                                <?php echo $this->Form->postLink(__('<i class="ti-trash"></i>'), array('action' => 'delete', $sale['Sale']['id']),array('class'=>'','escape'=>false), array('confirm' => __('Are you sure you want to delete # %s?'))); ?>
                                
                                </td>
                            </tr>
                        <?php endforeach; ?>
				  
				  					 
					</tbody>
			</table>            
		</div>
	</div>
			
</div>





