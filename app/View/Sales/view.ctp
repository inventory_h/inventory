<div class="sales view">
<h2><?php echo __('Sale'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($sale['Sale']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Invoicenumber'); ?></dt>
		<dd>
			<?php echo h($sale['Sale']['invoicenumber']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Salesdate'); ?></dt>
		<dd>
			<?php echo h($sale['Sale']['salesdate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($sale['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $sale['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Totalprice'); ?></dt>
		<dd>
			<?php echo h($sale['Sale']['totalprice']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($sale['Sale']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($sale['Sale']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale'), array('action' => 'edit', $sale['Sale']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale'), array('action' => 'delete', $sale['Sale']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $sale['Sale']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Sales'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accountinfos'), array('controller' => 'accountinfos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accountinfo'), array('controller' => 'accountinfos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Salesdetails'), array('controller' => 'salesdetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salesdetail'), array('controller' => 'salesdetails', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Accountinfos'); ?></h3>
	<?php if (!empty($sale['Accountinfo'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('AccountNo'); ?></th>
		<th><?php echo __('AccountDate'); ?></th>
		<th><?php echo __('Bankname'); ?></th>
		<th><?php echo __('Sale Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($sale['Accountinfo'] as $accountinfo): ?>
		<tr>
			<td><?php echo $accountinfo['id']; ?></td>
			<td><?php echo $accountinfo['accountNo']; ?></td>
			<td><?php echo $accountinfo['accountDate']; ?></td>
			<td><?php echo $accountinfo['bankname']; ?></td>
			<td><?php echo $accountinfo['sale_id']; ?></td>
			<td><?php echo $accountinfo['created']; ?></td>
			<td><?php echo $accountinfo['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'accountinfos', 'action' => 'view', $accountinfo['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'accountinfos', 'action' => 'edit', $accountinfo['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'accountinfos', 'action' => 'delete', $accountinfo['id']), array('confirm' => __('Are you sure you want to delete # %s?', $accountinfo['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Accountinfo'), array('controller' => 'accountinfos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Salesdetails'); ?></h3>
	<?php if (!empty($sale['Salesdetail'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sale Id'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Vat'); ?></th>
		<th><?php echo __('Unit Id'); ?></th>
		<th><?php echo __('Paymenttype Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($sale['Salesdetail'] as $salesdetail): ?>
		<tr>
			<td><?php echo $salesdetail['id']; ?></td>
			<td><?php echo $salesdetail['sale_id']; ?></td>
			<td><?php echo $salesdetail['product_id']; ?></td>
			<td><?php echo $salesdetail['qty']; ?></td>
			<td><?php echo $salesdetail['price']; ?></td>
			<td><?php echo $salesdetail['discount']; ?></td>
			<td><?php echo $salesdetail['vat']; ?></td>
			<td><?php echo $salesdetail['unit_id']; ?></td>
			<td><?php echo $salesdetail['paymenttype_id']; ?></td>
			<td><?php echo $salesdetail['created']; ?></td>
			<td><?php echo $salesdetail['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'salesdetails', 'action' => 'view', $salesdetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'salesdetails', 'action' => 'edit', $salesdetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'salesdetails', 'action' => 'delete', $salesdetail['id']), array('confirm' => __('Are you sure you want to delete # %s?', $salesdetail['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Salesdetail'), array('controller' => 'salesdetails', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
