
<script>
    var vr;
    function remove_file(id) {
        $('#total' + id).remove();
    }
    var i=0;
    function addMulti_file() {
            appPrt = $(".append_attach_file_part").html().replace(/VR/g, i);
            $(".append_attach_file_here").append(appPrt);
            i++;
       
    }
    
</script>

<style>
label{
	text-align: right;
	margin-top: 9px;
}
.form-horizontal .control-label {
  margin-bottom: 0;
  padding-top: 0px;
  text-align: right;
}
</style>


			<div class="append_attach_file_part col-md-12" style="display: none;">						
		            <div class="" id="totalVR" style="display: block;">	
		                		                		               		                
		                <div class="form-group">
	                    <label class="col-md-1">Item</label>
	                    <div class="col-md-2">
	                    	<?php echo $this->Form->input('Saledetail.VR.product_id',array('options'=>$products,'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
	                    </div>
	              		 <label class="col-md-1">Unit</label>
	                    <div class="col-md-2">
	                    	<?php echo $this->Form->input('Saledetail.VR.unit',array('options'=>$units,'type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
	                    </div>	
	                     <label class="col-md-1">Qty</label>
	                    <div class="col-md-1">
	                    	<?php echo $this->Form->input('Saledetail.VR.qty',array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
	                    </div>               	                   	               
	                
	                    <label class="col-md-1">Price</label>
	                    <div class="col-md-2">
	                    	<?php echo $this->Form->input('Saledetail.VR.price',array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
	                    </div>
	               		
	                    <div class="">
	                    	<input type="button" class="btn btn-default" value="--" onclick="remove_file(VR)" />
	                    </div>
	               		
	                   
	                </div>
		                	
		                
		            </div>	
		        </div>


<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
        <h4 class="header-title m-t-0 m-b-30">Sales</h4>
        <div class="row">
		<div class="col-lg-12">                
				<?php echo $this->Form->create('Purchase', array('class' => 'form-horizontal')); ?>
                    
                    <div class="form-group">
	                    <label class="col-md-1 control-label">Inv. No.</label>
	                    <div class="col-md-4">
	                    	<?php echo $this->Form->input('invoicenumber', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
	                    </div>
	                    <label class="col-md-2 control-label">Sales Date</label>
	                    <div class="col-md-4">
	                    	<?php echo $this->Form->input('salesdate', array('type' =>'text', 'label' => false, 'class' => 'form-control','id'=>'datepicker-autoclose')); ?>
	                    </div>
	                </div>
                    
                   <div class="form-group">
                        <label class="col-md-1 control-label">Pyament</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('paymenttype_id', array('type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                        <label class="col-md-2 control-label">Customer</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('customer_id', array('type' => 'select', 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>
                    
                    
                     <div class="form-group">
                       <label class="col-md-1">Item</label>
                        <div class="col-md-2">
                       	
                            <?php echo $this->Form->input('Saledetail.0.product_id', array('options'=>$products,'type' => 'select', 'label' =>false, 'class' => 'form-control')); ?>
                          
                        </div>
                      <label class="col-md-1">Unit</label>
                        <div class="col-md-2">
                        <?php echo $this->Form->input('Saledetail.0.unit', array('options'=>$units,'type' => 'select', 'label' =>false, 'class' => 'form-control')); ?>
                           
                        </div>
                        
                        <label class="col-md-1">Qty</label>
                        <div class="col-md-1">
                            <?php echo $this->Form->input('Saledetail.0.qty', array('type' => 'text', 'label' =>false, 'class' => 'form-control')); ?>
                        </div>
                        
                     <label class="col-md-1">Price</label>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('Saledetail.0.price', array('type' => 'text', 'label' =>false, 'class' => 'form-control')); ?>
                        </div>
                        
                   
                        <input type="button" class="btn btn-default bucketbutton" value="+" onclick="addMulti_file()" />
                    </div>
                           
                    
                     <div class="append_attach_file_here"></div>
                     <hr>
                     
                     <div class="form-group">
	                
	                <label class="col-md-9" style="text-align:right;">Total</label>
                        <div class="col-md-2">
                        <?php echo $this->Form->input('', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
                           
                        </div>
	                    
	                </div>
	                
	                <div class="form-group">
	                
	                <label class="col-md-9" style="text-align:right;">Discount</label>
                        <div class="col-md-2">
                        <?php echo $this->Form->input('discount', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
                           
                        </div>
	                    
	                </div>
	                <div class="form-group">
	                
	                <label class="col-md-9" style="text-align:right;">Vat</label>
                        <div class="col-md-2">
                       <?php echo $this->Form->input('vat', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
                           
                        </div>
	                    
	                </div>
	                <div class="form-group">
	                
	                <label class="col-md-9" style="text-align:right;">Grand Total</label>
                        <div class="col-md-2">
                        <?php echo $this->Form->input('totalprice', array('type' => 'text', 'label' => false, 'class' => 'form-control')); ?>
                           
                        </div>
	                   
	                </div>
                    
                    
					<div class="form-group">
	                    <label class="col-md-3 control-label"></label>
	                    <div class="col-md-9">
	                    	<button type="submit" class="btn btn-primary">Save</button>
	                    	<button type="reset" class="btn">Cancel</button>
	                    </div>
	                </div>
                <?php echo $this->Form->end(); ?> 
        	</div>
        	</div>
        </div>
	</div>
</div>
<script type="text/javascript">
	$( function() {
		jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    
  } );
  </script>



