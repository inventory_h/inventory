<div class="accountrecives form">
<?php echo $this->Form->create('Accountrecife'); ?>
	<fieldset>
		<legend><?php echo __('Edit Accountrecife'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('invoicenumber');
		echo $this->Form->input('amount');
		echo $this->Form->input('customer_id');
		echo $this->Form->input('paymentdate');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Accountrecife.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Accountrecife.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Accountrecives'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>
