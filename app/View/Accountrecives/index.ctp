<div class="accountrecives index">
	<h2><?php echo __('Accountrecives'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('invoicenumber'); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th>
			<th><?php echo $this->Paginator->sort('paymentdate'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($accountrecives as $accountrecife): ?>
	<tr>
		<td><?php echo h($accountrecife['Accountrecife']['id']); ?>&nbsp;</td>
		<td><?php echo h($accountrecife['Accountrecife']['invoicenumber']); ?>&nbsp;</td>
		<td><?php echo h($accountrecife['Accountrecife']['amount']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($accountrecife['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $accountrecife['Customer']['id'])); ?>
		</td>
		<td><?php echo h($accountrecife['Accountrecife']['paymentdate']); ?>&nbsp;</td>
		<td><?php echo h($accountrecife['Accountrecife']['created']); ?>&nbsp;</td>
		<td><?php echo h($accountrecife['Accountrecife']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $accountrecife['Accountrecife']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $accountrecife['Accountrecife']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $accountrecife['Accountrecife']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $accountrecife['Accountrecife']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Accountrecife'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>
