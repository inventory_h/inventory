<div class="accountrecives view">
<h2><?php echo __('Accountrecife'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($accountrecife['Accountrecife']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Invoicenumber'); ?></dt>
		<dd>
			<?php echo h($accountrecife['Accountrecife']['invoicenumber']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($accountrecife['Accountrecife']['amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($accountrecife['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $accountrecife['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Paymentdate'); ?></dt>
		<dd>
			<?php echo h($accountrecife['Accountrecife']['paymentdate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($accountrecife['Accountrecife']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($accountrecife['Accountrecife']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Accountrecife'), array('action' => 'edit', $accountrecife['Accountrecife']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Accountrecife'), array('action' => 'delete', $accountrecife['Accountrecife']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $accountrecife['Accountrecife']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Accountrecives'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accountrecife'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>
