<div class="paymenttypes view">
<h2><?php echo __('Paymenttype'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($paymenttype['Paymenttype']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($paymenttype['Paymenttype']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($paymenttype['Paymenttype']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($paymenttype['Paymenttype']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Paymenttype'), array('action' => 'edit', $paymenttype['Paymenttype']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Paymenttype'), array('action' => 'delete', $paymenttype['Paymenttype']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $paymenttype['Paymenttype']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Paymenttypes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paymenttype'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Purchases'), array('controller' => 'purchases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Purchase'), array('controller' => 'purchases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Salesdetails'), array('controller' => 'salesdetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salesdetail'), array('controller' => 'salesdetails', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Purchases'); ?></h3>
	<?php if (!empty($paymenttype['Purchase'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Invoicenumber'); ?></th>
		<th><?php echo __('Purchasedate'); ?></th>
		<th><?php echo __('Paymenttype Id'); ?></th>
		<th><?php echo __('Supplier Id'); ?></th>
		<th><?php echo __('Totalprice'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($paymenttype['Purchase'] as $purchase): ?>
		<tr>
			<td><?php echo $purchase['id']; ?></td>
			<td><?php echo $purchase['invoicenumber']; ?></td>
			<td><?php echo $purchase['purchasedate']; ?></td>
			<td><?php echo $purchase['paymenttype_id']; ?></td>
			<td><?php echo $purchase['supplier_id']; ?></td>
			<td><?php echo $purchase['totalprice']; ?></td>
			<td><?php echo $purchase['created']; ?></td>
			<td><?php echo $purchase['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'purchases', 'action' => 'view', $purchase['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'purchases', 'action' => 'edit', $purchase['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'purchases', 'action' => 'delete', $purchase['id']), array('confirm' => __('Are you sure you want to delete # %s?', $purchase['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Purchase'), array('controller' => 'purchases', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Salesdetails'); ?></h3>
	<?php if (!empty($paymenttype['Salesdetail'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sale Id'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Vat'); ?></th>
		<th><?php echo __('Unit Id'); ?></th>
		<th><?php echo __('Paymenttype Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($paymenttype['Salesdetail'] as $salesdetail): ?>
		<tr>
			<td><?php echo $salesdetail['id']; ?></td>
			<td><?php echo $salesdetail['sale_id']; ?></td>
			<td><?php echo $salesdetail['product_id']; ?></td>
			<td><?php echo $salesdetail['qty']; ?></td>
			<td><?php echo $salesdetail['price']; ?></td>
			<td><?php echo $salesdetail['discount']; ?></td>
			<td><?php echo $salesdetail['vat']; ?></td>
			<td><?php echo $salesdetail['unit_id']; ?></td>
			<td><?php echo $salesdetail['paymenttype_id']; ?></td>
			<td><?php echo $salesdetail['created']; ?></td>
			<td><?php echo $salesdetail['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'salesdetails', 'action' => 'view', $salesdetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'salesdetails', 'action' => 'edit', $salesdetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'salesdetails', 'action' => 'delete', $salesdetail['id']), array('confirm' => __('Are you sure you want to delete # %s?', $salesdetail['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Salesdetail'), array('controller' => 'salesdetails', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
