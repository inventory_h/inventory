<style type="text/css">
    body { background: url(../img/bg-login.jpg) !important; }
</style>


<div class="container-fluid-full">
    <div class="row-fluid">

        <div class="row-fluid">
            <div class="login-box">
                <h2>Create your account</h2>
                <?php echo $this->Form->create('User', array('class' => 'form-horizontal')); ?>
                <fieldset>

                    <div class="input-prepend" title="Username">
                        <span class="add-on"><i class="halflings-icon user"></i></span>
                        <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'input-large span10', 'id' => 'username', 'placeholder' => 'type username')); ?>
                    </div>
                    <div class="clearfix"></div>

                    <div class="input-prepend" title="Email">
                        <span class="add-on"><i class="halflings-icon lock"></i></span>
                        <?php echo $this->Form->input('email', array('label' => false, 'div' => false, 'type' => 'email', 'class' => 'input-large span10', 'id' => 'Email', 'placeholder' => 'type Email')); ?>
                    </div>
                    <div class="input-prepend" title="Mobile">
                        <span class="add-on"><i class="halflings-icon lock"></i></span>
                        <?php echo $this->Form->input('mobile', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'input-large span10', 'id' => 'Mobile', 'placeholder' => 'type Mobile')); ?>
                    </div>
                    <div class="input-prepend" title="username">
                        <span class="add-on"><i class="halflings-icon lock"></i></span>
                        <?php echo $this->Form->input('username', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'input-large span10', 'id' => 'username', 'placeholder' => 'type User name')); ?>
                    </div>
                    <div class="input-prepend" title="password">
                        <span class="add-on"><i class="halflings-icon lock"></i></span>
                        <?php echo $this->Form->input('password', array('label' => false, 'div' => false, 'type' => 'password', 'class' => 'input-large span10', 'id' => 'password', 'placeholder' => 'type password')); ?>
                    </div>
                    <div class="input-prepend" title="role">
                        <span class="add-on"><i class="halflings-icon lock"></i></span>
                        <?php echo $this->Form->input('role', array('type' => 'select', 'options' => $roles, 'label' => false, 'div' => false, 'class' => 'input-large span10', 'id' => 'role', 'placeholder' => 'type role')); ?>
                    </div>
                    <div class="input-prepend" title="status">
                        <span class="add-on"><i class="halflings-icon lock"></i></span>
                        <?php echo $this->Form->input('status', array('type' => 'select', 'options' => $status, 'label' => false, 'div' => false, 'class' => 'input-large span10', 'id' => 'status', 'placeholder' => 'type status')); ?>
                    </div>
                    <div class="clearfix"></div>

                    <label for="remember" class="remember"><?php echo $this->Html->link('Sign In', array('action' => 'login')); ?></label>

                    <div class="button-login">	
                        <button type="submit" class="btn btn-primary">Sign Up</button>
                    </div>
                    <div class="clearfix"></div>
                    <?php echo $this->Form->end(); ?>
                    <hr>
            </div><!--/span-->
        </div><!--/row-->


    </div><!--/.fluid-container-->

</div>
