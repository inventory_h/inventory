
<style>
    body { background: url(../img/bg-login.jpg) !important; }



    @import "bourbon";

    body {
        background: #eee !important;	
    }

    .wrapper {	
        margin-top: 80px;
        margin-bottom: 80px;
    }

    .form-signin {
        max-width: 380px;
        padding: 15px 35px 45px;
        margin: 0 auto;
        background-color: #fff;
        border: 1px solid rgba(0,0,0,0.1);
        box-shadow: 10px 10px 5px #888888;

        .form-signin-heading,
        .checkbox {
            margin-bottom: 30px;
        }

        .checkbox {
            font-weight: normal;
        }

        .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            @include box-sizing(border-box);

            &:focus {
                z-index: 2;
            }
        }

        input[type="text"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        input[type="password"] {
            margin-bottom: 20px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    }

</style>


<div class="wrapper">

        <div class="row-fluid">
            <div class="login-box">
                <?php echo $this->Form->create('User', array('class' => 'form-horizontal form-signin')); ?>
                <h2 class="form-signin-heading">Login to your account</h2>

                <div class="input-prepend" title="Username">
                    <span class="add-on"><i class="halflings-icon user"></i></span>
                    <?php echo $this->Form->input('username', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control input-large span10', 'id' => 'username', 'placeholder' => 'type username')); ?>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="input-prepend" title="Password">
                    <span class="add-on"><i class="halflings-icon lock"></i></span>
                    <?php echo $this->Form->input('password', array('label' => false, 'div' => false, 'type' => 'password', 'class' => 'form-control input-large span10', 'id' => 'password', 'placeholder' => 'type password')); ?>
                </div>
                <div class="clearfix"></div>

                <label for="remember" class="checkbox remember" style="clear: both; padding-left: 22px;"><div class="checker" id="uniform-remember"><span class=""><input type="checkbox" id="remember"></span></div>Remember me</label>

                <div class="button-login">	

                    <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
                </div>
                <div style="clear: both;"></div>
                <label for="remember" class="remember"><?php echo $this->Html->link('Create Account', array('action' => 'add')); ?></label>
                <h3 style="float: right; padding-right: 10px;">Forgot Password?</h3>
                <div class="clearfix"></div>
                <?php echo $this->Form->end(); ?>
                <hr>

            </div>
        </div>



</div>








<!--<div class="wrapper">
    <form class="form-signin">       
        <h2 class="form-signin-heading">Please login</h2>
        <input type="text" class="form-control" name="username" placeholder="Email Address" required="" autofocus="" />
        <input type="password" class="form-control" name="password" placeholder="Password" required=""/>      
        <label class="checkbox">
            <input type="checkbox" value="remember-me" id="rememberMe" name="rememberMe"> Remember me
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>   
    </form>
</div>-->

